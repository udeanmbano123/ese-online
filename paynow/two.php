<?php
function WereBackFromPaynow()
{
	global $integration_id;
	global $integration_key;
	global $checkout_url;
	global $orders_data_file;

	$local_order_id = $_GET['order_id'];
			
	//Lets get our locally saved settings for this order
	$orders_array = array();
	if (file_exists($orders_data_file))
	{
		$orders_array = parse_ini_file($orders_data_file, true);
	}
	
	$order_data = $orders_array['OrderNo_'.$local_order_id];
	
	$ch = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($ch, CURLOPT_URL, $order_data['pollurl']);
	curl_setopt($ch, CURLOPT_POST, 0);
	curl_setopt($ch, CURLOPT_POSTFIELDS, '');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	//execute post
	$result = curl_exec($ch);

	if($result) {

		//close connection
		$msg = ParseMsg($result);
		
		$MerchantKey =  $integration_key;
		$validateHash = CreateHash($msg, $MerchantKey);

		if($validateHash != $msg["hash"]){
			header("Location: $checkout_url");
		}
		else
		{
			/***** IMPORTANT ****
			On Paynow, payment status has changed, say from Awaiting Delivery to Delivered
			
				Here is where you
				1. Update your local shopping cart of Payment Status etc and do appropriate actions here, Save data to DB
				2. Email, SMS Notifications to customer, merchant etc
				3. Any other thing
			
			*** END OF IMPORTANT ****/
			//1. Lets write the updated settings
			$orders_array['OrderNo_'.$local_order_id] = $msg;
			$orders_array['OrderNo_'.$local_order_id]['returned_from_paynow'] = 'yes';
			
			write_ini_file($orders_array, $orders_data_file, true);	
		}
	}
	
	//Thank	your customer
	getBackFromPaynowHTML();
}