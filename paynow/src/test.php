<?php

require 'vendor/autoload.php';

$p = new Paynow\Paynow(0000, 'abcdef');

$reference = '123';
$amount = 10.00;
$additionalInfo = 'Payment for order '.$reference;
$returnUrl = 'http://example.com/thankyou';
$resultUrl = 'http://example.com/result';

$res = $p->initiatePayment(
    $reference,
    $amount,
    $additionalInfo,
    $returnUrl,
    $resultUrl
);

echo "<a href='".$res->browserurl."'>Make payment</a>";

?>