<?php

/**
 * Handling database connection
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class DbConnectATS {

    private $conn;

    function __construct() {        
    }

    /**
     * Establishing database connection
     * @return database connection handler
     */
    function connect() {
        include_once dirname(__FILE__) . '/Configa.php';

        $connectionInfo = array("Database"=>ATS_MSSQL_DB_NAME);
        // Connecting to mysql database
        $this->conn = sqlsrv_connect(ATS_MSSQL_DB_HOST, $connectionInfo);
        // $this->conn = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
// echo  "no error" ; 
        // Check for database connection error
        if( $this->conn === false )
        {
             echo "Unable to connect.</br>";
             die( print_r( sqlsrv_errors(), true));
        }
        // if (mysqli_connect_errno()) {
        //     echo "Failed to connect to MySQL: " . mysqli_connect_error();
        // }

        // returing connection resource
        return $this->conn;
    }

}

?>
