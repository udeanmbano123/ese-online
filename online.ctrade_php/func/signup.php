<?php
include("../db/DbConnect.php");
require_once '../db/PassHash.php';

$my_name  = $_POST['my_name'] ;
$cds_number  = $_POST['cds_number'] ;
$my_email  = $_POST['my_email'] ;
$my_pass  = $_POST['my_pass'] ;
$db = new DbConnect();
$conn = $db->connect();

if (!isUserExists($conn ,$my_email, $cds_number)) {

	$pass =   PassHash::hash($my_pass);; 
	$api_key = PassHash::generateApiKey();

	$sql = "INSERT INTO users
	(id,
	name,
	email,
	password_hash,
	api_key,
	status,
	cds_no,
	user_type)
	VALUES
	(NULL,
	'".$my_name."',
	'".$my_email."',
	'".$pass."',
	'".$api_key."',
	0,
	'".$cds_number."',
	'online_trading');
	";

	// echo  $sql ; 
	if ($conn->query($sql) === TRUE) {
		sendEmail($my_email , $api_key , $my_name , $my_pass) ; 
		header("Location: ../login.php?login_after=true") ; 
	} else {
	   echo "Error: " . $sql . "<br>" . $conn->error;
	}

} else {
	header("Location: ../login.php?sms_err=Email or CDS Number already exists") ; 
}

function isUserExists($consn , $email , $cdno) {
    $sqll ="SELECT id from users WHERE email = '".$email."' or cds_no  = '".$cdno."' ";
    // echo $sqll
    $result = mysqli_query($consn,$sqll);
    $num_rows = mysqli_num_rows($result);
    return $num_rows > 0;
}
function sendEmail($email  , $verify_code , $name , $password){
include("../sendmail.php") ;

$mailerThis = new sendMail() ;

$message =<<<EEF
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <title></title>
  <!--[if !mso]><!-- -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--<![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
  #outlook a { padding: 0; }
  .ReadMsgBody { width: 100%; }
  .ExternalClass { width: 100%; }
  .ExternalClass * { line-height:100%; }
  body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
  table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
  img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
  p { display: block; margin: 13px 0; }
</style>
<!--[if !mso]><!-->
<style type="text/css">
  @media only screen and (max-width:480px) {
    @-ms-viewport { width:320px; }
    @viewport { width:320px; }
  }
</style>
<!--<![endif]-->
<!--[if mso]>
<xml>
  <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
  </o:OfficeDocumentSettings>
</xml>
<![endif]-->
<!--[if lte mso 11]>
<style type="text/css">
  .outlook-group-fix {
    width:100% !important;
  }
</style>
<![endif]-->

<!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
    <style type="text/css">

        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);

    </style>
  <!--<![endif]--><style type="text/css">
  @media only screen and (min-width:480px) {
    .mj-column-per-100 { width:100%!important; }
  }
</style>
<style type="text/css">
      @media only screen and (max-width:480px) {
        .mj-hero-content {
          width: 100% !important;
        }
      }
    </style></head>
<body style="background: #f5f6fa;">
  <div style="background-color:#f5f6fa;"><!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;padding-bottom:20px;padding-top:30px;"><!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:undefined;width:600px;">
      <![endif]--><div class="" style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:40px;text-align:center;">
                    <a href="#" class="ks-logo" style="font-size: 40px; text-decoration: none; color: #5ea6ca; font-weight: bold;padding:40px;"><img src="http://finsec.co.zw/onlinetrade/img/finlogo.png" width="50%"></a>
                </div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]-->
      <!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]--><!--[if mso | IE]>
          <v:image xmlns:v="urn:schemas-microsoft-com:vml" croptop="0" cropbottom="0" style="width:435pt;height:180pt;position:absolute;top:0;mso-position-horizontal:center;border:0;z-index:-3;" src="../admin/default-primary/assets/img/placeholders/placeholder-640x480.png" />
        <![endif]--><!--[if mso | IE]>
      </td></tr></table>
      <![endif]-->
      <!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]--><div style="margin:0px auto;max-width:600px;background:#fff;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#fff;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;"><!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
      <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:20px 40px;" align="center"><div class="" style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:40px;text-align:center;">
                        <h2 class="ks-header-h2" style="font-size: 20px; font-weight: 500; color: #333; margin-top: 0; margin-bottom: 0;">
                            Welcome to the C-TRADE Online Trading Platform
                        </h2>
                    </div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="left"><div class="" style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;">
                        <p style="font-size: 14px; color: #333; margin: 5px 0;">Hi {$name}, welcome to C-TRADE online trading platform .</p>

                        <p style="font-size: 14px; color: #333; margin: 5px 0;">
                            Please find below your login details;<br>
                              <div style="margin:40px;">
                                Email  - {$email}<br>
                                Password  - {$password}<br>
                                Web address - http://finseczim.com/ctrade/login.php<br>
                              </div>
                          <b>NB: </b><i>Please note you will only be able to login after verifying your account by clicking on verify account button below </i>
                        </p>                        
                    </div></td></tr>
                    <tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;padding-top:20px;padding-bottom:20px;"><p style="color: #333; font-size: 1px; margin: 0px auto; border-top: 1px solid #e6e6e6; width: 100%;"></p><!--[if mso | IE]><table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" style="font-size:1px;margin:0px auto;border-top:1px solid #e6e6e6;width:100%;" width="600"><tr><td style="height:0;line-height:0;"> </td></tr></table><![endif]--></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;">
                        <h3 class="ks-header-h3" style="font-size: 24px; font-weight: 500; color: #333; margin-top: 0; margin-bottom: 10px;">
                            CDS Number Verification and Login
                        </h3>
                        <p style="font-size: 14px; color: #333; margin: 5px 0;">
                            Please verify your account <br>
                        </p>
                    </div></td></tr>
                    <tr><td style="word-break:break-word;font-size:0px;padding:20px 40px;padding-bottom:30px;" align="left"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:separate;" align="left" border="0"><tbody><tr><td style="border:2px solid #5ea6ca;border-radius:2px;color:#5ea6ca;cursor:auto;padding:20px 40px;" align="center" valign="middle" bgcolor="#fff"><a href="http://finseczim.com/ctrade/verify.php?id={$verify_code}" style="text-decoration:none;line-height:100%;background:#fff;color:#5ea6ca;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:14px;font-weight:500;text-transform:none;margin:0px;" target="_blank">
                        Verify your account
                    </a></td></tr></tbody></table></td></tr>



                    <tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;padding-bottom:20px;"><p style="color: #333; font-size: 1px; margin: 0px auto; border-top: 1px solid #e6e6e6; width: 100%;"></p><!--[if mso | IE]><table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" style="font-size:1px;margin:0px auto;border-top:1px solid #e6e6e6;width:100%;" width="600"><tr><td style="height:0;line-height:0;"> </td></tr></table><![endif]--></td></tr>



                    </tbody></table></div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]-->
      <!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]--><div style="margin:0px auto;max-width:600px;background:#2ecc71;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#2ecc71;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;"><!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:undefined;width:600px;">
      <![endif]--><div class="" style="cursor:auto;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:18px;line-height:22px;text-align:center;">
                    Great to have you on board {$name} :)
                </div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]-->
      <!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]--><div style="margin:0px auto;max-width:600px;"></div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]--></div>
</body>
</html>
EEF;



$sendMail = $mailerThis->sendEmail($email, $message, 'Welcome to C-TRADE Online Trading Platform ' , $name );

// $headers = "From: no-reply@finsec.co.zw\r\n";
// $headers .= "Reply-To: no-reply@finsec.co.zw\r\n";
// $headers .= "BCC: tinashe@finsec.co.zw\r\n";
// $headers .= "MIME-Version: 1.0\r\n";
// $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

// mail($email, $subject, $message, $headers);

}

?>