
<?php

sendEmail_($_POST['my_email'] , $_POST['cds_number'] , $_POST['my_name'] , $_POST['my_pass']  ,$_POST['broker_send'] , $_POST['custodian_send']) ; 

function sendEmail_($email  , $verify_code , $name , $password ,$broker_send , $custodian_send){
  require_once("../sendmail.php") ;

  $mailerThis = new sendMail() ;

$message =<<<EEF

Dear {$name} 

<br><br>
The Securities and Exchange Commission of Zimbabwe (SECZ) is conducting a C-Trade post implementation survey aimed at collecting information relating to stakeholder experiences with the USSD, Mobile and Online platforms. 
<br><br>
The survey is anonymous and does not require the identity of respondents. Although responses are on a purely voluntary basis, the SECZ encourages stakeholders to respond to the questionnaire as this will help in identifying areas that require improvement. 
Your cooperation in completing the survey form is greatly appreciated. Below is the link to the survey.
<br>
<br>
https://goo.gl/forms/6mIUUDYq6zdMEkor1
<br>
<br>
Regards 
<br>
C-TRADE


EEF;



  $sendMail = $mailerThis->sendEmail($email, $message, 'C-Trade USER EXPERIENCE SURVEY' , $name );

  }


?>