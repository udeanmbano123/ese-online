<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C-TRADE</title>

        <!-- Vendor CSS -->
        <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
        <link href="vendors/bower_components/google-material-color/dist/palette.css" rel="stylesheet">

        <link href="vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="vendors/bower_components/chosen/chosen.min.css" rel="stylesheet">
        <link href="vendors/summernote/dist/summernote.css" rel="stylesheet">
        <link rel="icon" href="https://ctrade.co.zw/wp-content/uploads/2018/02/favicon-150x150.png" sizes="32x32" />
        <link rel="icon" href="https://ctrade.co.zw/wp-content/uploads/2018/02/favicon.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="https://ctrade.co.zw/wp-content/uploads/2018/02/favicon.png" />
        <!-- CSS -->
        <link href="css/style.css" rel="stylesheet">
        <link href="css/app.min.1.css" rel="stylesheet">
        <link href="css/app.min.2.css" rel="stylesheet">
        <link href="css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="css/buttons.dataTables.min.css" rel="stylesheet">
        
        <!-- Following CSS are used only for the Demp purposes thus you can remove this anytime. -->
        <style type="text/css">
            .toggle-switch .ts-label {
                min-width: 130px;
            }
            .error{
                color: #F44336 ; 
            }
        </style>
            <!-- CSS -->
            <link href="css/bootstrap.css" rel="stylesheet">
            <link href="css/style.css" rel="stylesheet">
            <link href="font-awesome/css/font-awesome.css" rel="stylesheet" >
            <link href="css/socialize-bookmarks.css" rel="stylesheet">
            <link href="js/fancybox/source/jquery.fancybox.css?v=2.1.4" rel="stylesheet">
            <link href="check_radio/skins/square/aero.css" rel="stylesheet">
            <!-- Toggle Switch -->
            <link rel="stylesheet" href="css/jquery.switch.css">
            <!-- Owl Carousel Assets -->
            <link href="css/owl.carousel.css" rel="stylesheet">
            <link href="css/owl.theme.css" rel="stylesheet">
            <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
            <!-- Jquery -->
            <script src="js/jquery-1.11.1.js"></script>
            <script src="js/jquery-ui-1.8.22.min.js"></script>
            <!-- Wizard-->
            <script src="js/jquery.wizard.js"></script>
            <!-- Radio and checkbox styles -->
            <script src="check_radio/jquery.icheck.js"></script>
            <!-- HTML5 and CSS3-in older browsers-->
            <script src="js/modernizr.custom.17475.js"></script>
            <!-- Support media queries for IE8 -->
            <script src="js/respond.min.js"></script>        
        
</head>


    <body> 

        <header id="header" class="clearfix">
            <ul class="h-inner">
                <li class="hidden-xs">
                    <a href="../index.php" class="p-l-10">
                        <img src="img/demo/logo.png" style="width: 34%;" alt="">
                    </a>
                </li>

                <li class="pull-right">
                    <ul class="hi-menu">


                        <li class="dropdown" >
                            <a data-toggle="dropdown" href=""><span class="him-label" style ="color:#fff;">Guest</span></a>
                        </li>
                    </ul>
                </li>
            </ul>

        </header>

        
        <section id="main">

            <section id="content" >
                <div class="container" style="margin-top: 100px;">

                   <div id="survey_container">

                        <div id="top-wizard">
                          <strong>Progress <span id="location"></span></strong>
                          <div id="progressbar"></div>
                          <div class="shadow"></div>
                        </div>  

                        <div class="row"> 

                            <div class="col-sm-12 col-xs-6" >

                                        <!-- Form -->
                                        <div class="card" id ="place_order">
                                            
                                            <div class="card-header ch-alt">
                                                <h1 style="text-align:left;">
                                                    Create Account </h1>
                                                <h2>
                                                    <small>Create a cds account </small>
                                                </h2>
                                            </div>

                                            <div class="card-body card-padding">

                                                <form name  = "invoice" id="wrappeddddd" action = "" method = "POST">
                                                    <div class="row">
                                                      <div id="middle-wizard">

                                                         
                                                            <div class="step">                                                    
                                                                <input type ="hidden" name= "acc_type" value="i" >
                                                                <div class="col-sm-12">
                                                                    <div class="row">
                                                                        <div class="col-sm-4  ">
                                                                            <label class="c-blue" >Title</label>
                                                                            <select name  = "my_title" class="selectpicker required" >
                                                                                <option value = "Mr">Mr</option>
                                                                                <option value = "Mrs">Mrs</option>
                                                                                <option value = "Mrs">Miss</option>
                                                                                <option value = "Mrs">Ms.</option>
                                                                                <option value = "Mrs">Sir.</option>
                                                                                <option value = "Mrs">Esq.</option>
                                                                                <option value = "Mrs">Rev.</option>
                                                                                <option value = "Mrs">Dr.</option>
                                                                                <option value = "Mrs">Prof.</option>
                                                                            </select>
                                                                        </div>  


                                                                        <div class="col-sm-4 m-b-20">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Forenames <br></label>
                                                                                <input type="text" name  = "fore_name" id = "fore_name" class="form-control required letterswithbasicpunc"  placeholder="Forenames">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4 m-b-20">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Surname <br></label>
                                                                                <input type="text" name  = "surname" id = "surname" class="form-control required letterswithbasicpunc"  placeholder="Surname">
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                </div> 

                                                                <div class="col-sm-12">
                                                                    <div class="row">

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Date of Birth <br></label>
                                                                                <input type="text" name="dob" id="dob" data-date-start-date="-120y" data-date-end-date="-18y"  class="form-control required date-picker valid" placeholder="Click here..." aria-required="true" aria-invalid="false">
                                                                                <!-- <input type="text" name  = "" id = "my_price" class="form-control "  placeholder="Date of Birth"> -->
                                                                            </div>
                                                                        </div> 
                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Gender </label><br>
                                                                                <input type="radio" class ="required" name="gender" value="Male"  >
                                                                                <label for="compete_no">Male</label>

                                                                                <input type="radio" class ="required" name="gender" value="Female">
                                                                                <label for="compete_yes">Female</label>
                                                                            </div>
                                                                        </div>  

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">National ID<br></label>
                                                                                <input type="text" name  = "nat_id" id = "nat_id" class="form-control required"  placeholder="National ID">
                                                                            </div>
                                                                        </div>    
                                                                
                                                                    </div>    
                                                                </div> 
              

                                                                <div class="col-sm-12">
                                                                    <div class="row">

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Password<br></label>
                                                                                <input type="password" name  = "my_pass" id = "pass" class="form-control required"  placeholder="Password">
                                                                            </div>
                                                                        </div> 
                                                                        <div class="col-sm-8  ">

                                                                        </div> 
               
                                                                    </div>     
                                                                </div>   

                                                                <div class="col-sm-12">
                                                                    <!-- /Row -->   
                                                                    <div class="row">
                                                                       <div class="col-md-4 col-md-offset-4">
                                                                          <ul class="data-list" id="terms">
                                                                             <li>
                                                                                <strong>Do you accept <a href="https://ctrade.co.zw/online/docs/terms.pdf" target="_blank" > terms and conditions</a> ?</strong>
                                                                                <label class="switch-light switch-ios ">
                                                                                <input type="checkbox" name="terms" class="form-control  required fix_ie8" value="yes">
                                                                                <span>
                                                                                <span class="ie8_hide">No</span>
                                                                                <span>Yes</span>
                                                                                </span>
                                                                                <a></a>
                                                                                </label>
                                                                             </li>
                                                                          </ul>
                                                                       </div>
                                                                    </div>                                                            
                                                                </div>
                                                            </div>

                                                            <div class="step"> 
                                                                <div class="col-sm-12">
                                                                    Location <br>
                                                                    <div class="row">

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Address-Home<br></label>
                                                                                <input type="text" name  = "add_home" id = "add" class="form-control required"  placeholder="Address-Home">
                                                                            </div>
                                                                        </div> 

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Address-Business <br></label>
                                                                                <input type="text" name  = "add_bus" id = "addr" class="form-control required"  placeholder="Address-Business1">
                                                                            </div>
                                                                        </div>     
                                                                    </div>     
                                                                </div>  


                                                                <div class="col-sm-12">
                                                                    <div class="row">

                                                                        <div class="col-sm-4  ">

                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Country <br></label>
                                                                                <select name  = "country" class="selectpicker required" id="cont" data-live-search="true" title="Select Country ">
                                                                                   <?php include("cont.php") ; ?>
                                                                                </select>                                                                            
                                                                                <!-- <input type="text" name  = "" id = "my_price1" class="form-control "  placeholder="Country"> -->
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">City<br></label>
                                                                                <input type="text" name  = "city" id = "city" class="form-control required"  placeholder="City">
                                                                            </div>
                                                                        </div> 

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Mobile Number <br></label>
                                                                                <input type="text" name  = "cell_num" id = "mobile" class="form-control required"  placeholder="Mobile Number ">
                                                                            </div>
                                                                        </div>

                                                                    </div>     
                                                                </div>     

                                                                <div class="col-sm-12">
                                                                    <div class="row">

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Telephone<br></label>
                                                                                <input type="text" name  = "telphone" id = "tele" class="form-control required"  placeholder="Telephone">
                                                                            </div>
                                                                        </div> 

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Email <br></label>
                                                                                <input type="email" name  = "my_email" id = "email" class="form-control required"  placeholder="Email">
                                                                            </div>
                                                                        </div>     
                                                                    </div>     
                                                                </div>  
                                                            </div>

                                                            
                                                            <div class="step">  

                                                                <div class="col-sm-12">
                                                                    Banking Details <br>
                                                                    <div class="row">

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Bank Name<br></label>
                                                                                <select name  = "bank_name" class="selectpicker required" data-live-search="true"   id ="bank_name" title="Please Select Bank ">
                                                                                   <?php include("banks.php") ; ?>
                                                                                </select>     

                                                                                <!-- <input type="text" name  = "" id = "my_price1" class="form-control "  placeholder="Bank Name"> -->
                                                                            </div>
                                                                        </div> 

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Branch <br></label>

                                                                                <select name  = "branch" class="selectpicker required" id ="branch">
                                                                                  <option value = "0">  Please Select bank first  </option>
                                                                                </select>                                                                                 
                                                                                <!-- <input type="text" name  = "" id = "" class="form-control required"  placeholder="Branch"> -->
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Account Number <br></label>
                                                                                <input type="text" name  = "acc_num" id = "acc_num" class="form-control required"  placeholder="Account Number">
                                                                                 <input type ="hidden" name  = "stock_brocker" value = "WESTS" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-6">
                                                                        
                                                                        </div>


                                                                    </div>     
                                                                </div>     

                                                                <div class="col-sm-12">
                                                                    <div class="row">



                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Custodian<br></label>
                                                                                    <select name  = "custodian" class="selectpicker required" data-live-search="true" id ="custodian">
                                                                                        <?php include("getCustodian.php") ; ?>
                                                                                    </select>                                                                    
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-4  ">
<!--                                                                             <div class="form-group fg-line">
                                                                                <label  class="c-blue">Stockbrocking Firm<br></label>
                                                                                    <select name  = "stock_brocker" class="selectpicker required" data-live-search="true" id ="stockbrocker">
                                                                                        <option value = "ABCS">   ABC STOCKBROKERS  </option>
                                                                                        <option value = "AKRI">   AKRIBOS SECURITIES (PVT) LTD  </option>
                                                                                        <option value = "AutoInvest">   Auto Investments  </option>
                                                                                        <option value = "BEAR">   BEAR SECURITIES   </option>
                                                                                        <option value = "BETHEL">   BETHEL EQUITIES P/L  </option>
                                                                                        <option value = "CORP">   CORPSERVE REGISTRARS  </option>
                                                                                        <option value = "DELL">   DELL STOCKBROKERS  </option>
                                                                                        <option value = "EFES">   EFE SECURITIES (PVT) LTD  </option>
                                                                                        <option value = "EGS">   Eagle Stockbrokers  </option>
                                                                                        <option value = "EST">   EASTGATE STOCKBROKERS  </option>
                                                                                        <option value = "FBCS">   FBC SECURITIES (PVT) LTD  </option>
                                                                                        <option value = "FCN">   FALCON STOCKBROKERS  </option>
                                                                                        <option value = "FIN">   FINSEC  </option>
                                                                                        <option value = "IMARA">   IMARA EDWARDS SECURITIES (PVT) LTD  </option>
                                                                                        <option value = "INVS">   INVICTUS SECURITIES ZIMBABWE (PVT) LTD  </option>
                                                                                        <option value = "LES">   LYNTON-EDWARDS STOCKBROKERS(PVT) LTD  </option>
                                                                                        <option value = "MASTS">   MAST STOCKBROKERS (PVT) LTD  </option>
                                                                                        <option value = "MMCS">   MMC STOCKBROKERS  </option>
                                                                                        <option value = "OMCUS">   OLD MUTUAL CUSTODIAL SERVICES  </option>
                                                                                        <option value = "OMSEC">   OLD MUTUAL SECURITIES (PVT) LTD  </option>
                                                                                        <option value = "PLATS">   PLATINUM SECURITIES (PVT) LTD  </option>
                                                                                        <option value = "PLT">   PALLET  </option>
                                                                                        <option value = "SECZ">   Securities Exchange Commission Zimbabwe  </option>
                                                                                        <option value = "STL">   Stallone Securities  </option>
                                                                                        <option value = "STS">   Southern Trust Securities  </option>
                                                                                        <option value = "TAAA">   Tripple Alliance Brokers  </option>
                                                                                        <option value = "WESTS">   WEST STOCKBROKERS  </option>
                                                                                        <option value = "WHSEC">   WATERHOUSE SECURITIES  </option>
                                                                                    </select>                                                                    
                                                                            </div> -->
                                                                        </div> 

                                                                        <div class="col-sm-4  ">

                                                                        </div>

                                                                    </div>     
                                                                </div>     


                                                                <div class="clearfix"></div>

                                                            </div>

                                                             <!-- end step-->
                                                             <div class="submit step" id="complete">
                                                                <i class="icon-check"></i>
                                                                <h3>Your application is now ready for submission. </h3>
                                                                <button type="submit" name="process" class="submit">Submit the registration details</button>                                                             
                                                             </div>
                                                             <!-- end submit step -->
                                                             <!-- </div> -->
                                                             <!-- end middle-wizard -->
                                                             <div id="bottom-wizard">
                                                                <button type="button" name="backward" class="backward">Back</button>
                                                                <button type="button" name="forward" class="forward">Next</button>

                                                             </div>
                                                             <!-- end bottom-wizard -->


                                                      </div>
                                                    </div>
                                                </form>

                                            </div>

                                        </div>

                            </div>

                        </div>


                    </div>
                </div>
            </section>

<div class="modalz"></div>
           
<div id="modalConfirmYesNo" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" 

                class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="lblTitleConfirmYesNo" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p id="lblMsgConfirmYesNo"></p>
            </div>
            <div class="modal-footer">
                <button id="btnYesConfirmYesNo" 

                type="button" class="btn btn-primary">Proceed to login</button>
                <button id="btnNoConfirmYesNo" 

                type="button" class="btn btn-default">New Registration</button>
            </div>
        </div>
    </div>
</div>

        </section>

       <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        
        <script src="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="vendors/bootstrap-growl/bootstrap-growl.min.js"></script>

        <script src="vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>
        <script src="vendors/summernote/dist/summernote-updated.min.js"></script>
        
        <script src="vendors/bower_components/chosen/chosen.jquery.min.js"></script>
        <script src="vendors/fileinput/fileinput.min.js"></script>
        <script src="vendors/input-mask/input-mask.min.js"></script>
        <script src="vendors/farbtastic/farbtastic.min.js"></script>
        <script src="js/jqClock.min.js"></script>

        <script src="js/functions.js"></script>
        <script src="js/actions.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/dataTables.buttons.min.js"></script>
        <script src="js/buttons.print.min.js"></script>
        <script src="js/demo.js"></script>
        <script src="js/jquery.validate.js"></script>
        <script src="js/additional-methods.js"></script>
        <script type="text/javascript">
            function AsyncConfirmYesNo(title, msg, yesFn, noFn) {
                var $confirm = $("#modalConfirmYesNo");
                $confirm.modal('show');
                $("#lblTitleConfirmYesNo").html(title);
                $("#lblMsgConfirmYesNo").html(msg);
                $("#btnYesConfirmYesNo").off('click').click(function () {
                    yesFn();
                    $confirm.modal("hide");
                });
                $("#btnNoConfirmYesNo").off('click').click(function () {
                    noFn();
                    $confirm.modal("hide");
                });
            }
function ShowConfirmYesNo(message) {
    AsyncConfirmYesNo(
        "Account Creation",
        message,
        MyYesFunction,
        MyNoFunction
    );
}            

function MyYesFunction() {
    alert("Login to trade now");
    // $("#lblTestResult").html("You are about to exit ");
    window.location.href = "http://trading.ssx.co.sz:88/ctrade/";
}
function MyNoFunction() {
    alert("Create new account");
    window.location.href = "http://trading.ssx.co.sz:88/ctrade/online.ctrade_php/sign_up.php";
    // $("#lblTestResult").html("You are not hungry");
}
         $(document).ready(function(){  

<?php
if(isset($_GET['message'])){
    echo "ShowConfirmYesNo('".$_GET['message']."') ;" ; 
}
?>

                        $body = $("body");

                        $(document).on({
                             ajaxStart: function() { $body.addClass("loading");    },
                             ajaxStop: function() { $body.removeClass("loading"); }    
                        }); 

                        // $("#post-btn-nooo").click(function(){        
                        //     $.post("func/create_acc.php", $("#wrappeddddd").serialize(), function(data) {
                        //         // alert(data);
                        //         ShowConfirmYesNo(data) ;
                        //     });
                        // });

                        var val_yeduw ;
                        var text ;  
                        $('.selectpicker').selectpicker({
                          size: 10
                        });
                        $('select[name="bank_name"]').change(function() {
                            val_yeduw = $(this).val() ; 
                            //$('select[name="branch"]').selectpicker('refresh');
                            // alert(val_yeduw) ; 
                            $("#branch option").each(function() {
                                $(this).remove();
                            });
                            $('select[name="branch"]').selectpicker('refresh');
                            $.ajax({
                                type: "GET",
                                url: "getBranch.php",
                                /*or any server side url which generate citylist for rails
                                         "/states/city_list"or<%= city_list_states_path%> */
                                data: "bank="+val_yeduw,
                               /*here we are passing state_id to server based on that on
                                     he can find out  all the city related to that states */
                                success: function(html){
                                    /*html content response of city_list.xxx */
                                    $("#branch").html(html);
                                    $('select[name="branch"]').selectpicker('refresh');
                                }
                            });

                        });

                     
            
            });     
        </script>
            <script src="js/jquery.tweet.min.js"></script>
            <script src="js/jquery.bxslider.min.js"></script>
            <script src="js/quantity-bt.js"></script>

            <script src="js/retina.js"></script>
            <script src="js/owl.carousel.min.js"></script>
            <script src="js/function_s.js"></script>
            <script src="js/function_f.js"></script>
            <!-- FANCYBOX -->
            <script  src="js/fancybox/source/jquery.fancybox.pack.js?v=2.1.4" type="text/javascript"></script> 
            <script src="js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5" type="text/javascript"></script> 
            <script src="js/fancy_func.js" type="text/javascript"></script> 

    </body>
  
</html>