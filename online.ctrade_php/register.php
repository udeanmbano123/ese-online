<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en">
   <![endif]-->
   <!--[if IE 9 ]>
   <html class="ie ie9" lang="en">
      <![endif]-->
      <html lang="en">
         <!--<![endif]-->
         <head>
            <!-- Basic Page Needs -->
            <meta charset="utf-8">
            <title>C-TRADE</title>
            <meta name="description" content="">
            <meta name="author" content="Ansonika">
            <!-- Mobile Specific Metas -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <!-- CSS -->
            <link href="css/bootstrap.css" rel="stylesheet">
            <link href="css/style.css" rel="stylesheet">
            <link href="font-awesome/css/font-awesome.css" rel="stylesheet" >
            <link href="css/socialize-bookmarks.css" rel="stylesheet">
            <link href="js/fancybox/source/jquery.fancybox.css?v=2.1.4" rel="stylesheet">
            <link href="check_radio/skins/square/aero.css" rel="stylesheet">
            <!-- Toggle Switch -->
            <link rel="stylesheet" href="css/jquery.switch.css">
            <!-- Owl Carousel Assets -->
            <link href="css/owl.carousel.css" rel="stylesheet">
            <link href="css/owl.theme.css" rel="stylesheet">
            <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
            <!-- Jquery -->
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/jquery-ui-1.8.22.min.js"></script>
            <!-- Wizard-->
            <script src="js/jquery.wizard.js"></script>
            <!-- Radio and checkbox styles -->
            <script src="check_radio/jquery.icheck.js"></script>
            <!-- HTML5 and CSS3-in older browsers-->
            <script src="js/modernizr.custom.17475.js"></script>
            <!-- Support media queries for IE8 -->
            <script src="js/respond.min.js"></script>
         </head>
         <body>
            <section id="top-area">
               <header id="header" class="clearfix" data-ma-theme="blue">
                  <ul class="h-inner">
                     <li class="hi-logo hidden-xs">
                        <a href="#">C-TRADE</a>
                     </li>
                     <li class="pull-right">
                        <ul class="hi-menu">
                           <li>
                              <a href="login.php">Login  | </a>
                           </li>
                           <li>
                              <a href="#">Create Account</a>
                           </li>
                        </ul>
                     </li>
                  </ul>
               </header>
               <div class="container">
                  <div class="row">
                     <div class="col-md-12 main-title">
                        <h1>Register</h1>
                     </div>
                  </div>
               </div>
            </section>
            <section class="container" id="main">
               <!-- Start Survey container -->
               <div id="survey_container">
               <div id="top-wizard">
                  <strong>Progress <span id="location"></span></strong>
                  <div id="progressbar"></div>
                  <div class="shadow"></div>
               </div>
               <!-- end top-wizard -->
               <form name="example-1" id="wrapped" action="" method="POST">
                  <div id="middle-wizard">
                     <div class="step">
                        <!-- Row -->
                        <div class="row">
                           <!-- Col -->
                           <div class="col-md-6 mgb-30-xs">
                              <h3>
                                 Personal Details
                              </h3>

                              <!-- Form Group -->
                              <div class="form-group">
                                 <label>                              
                                       Depository
                                 </label>
                                       <select name="depository"  class="form-control" >
                                        <option value="0">Please Select a Depository</option>
                                        <option value="ABC">ABACUS</option>
                                       </select>

                              </div>
                              <!-- Form Group -->
                              <div class="form-group">
                                 <label>
                                       Account Type*  <br>  
                                 </label>                                                                                
                                       <input type="radio" name="acc_type" id="acc_type" value="LOCAL" /><label for="acc_type">EXSISTING</label>
                                       <input id="acc_type_1" type="radio" name="acc_type" value="GLOBAL" /><label for="acc_type_1">NEW</label>
                              </div>

                              <!-- Form Group -->
                              <div class="form-group">
                                 <label>
                                    Application Type* <br>
                                 </label>
                                    <input type="radio" name="app_type" id="app_type" value="LOCAL" /><label for="app_type">COMPANY</label>
                                    <input id="app_type_1" type="radio" name="app_type" value="GLOBAL" /><label for="app_type_1">INDIVIDUAL</label>
                              </div>
                              <!-- Form Group -->
                              <div class="form-group">
                                 <label>
                                       Title
                                 </label>
                                       <select name="my_titles" id="my_titles" class="form-control">
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Miss">Miss</option>
                                        <option value="Dr">Dr</option>
                                        <option value="Rev">Rev</option>
                                       </select>
                              </div>

                              <!-- Form Group -->
                              <div class="form-group">
                                 <label>
                                    Initials
                                 </label>
                                 <input name="initials" type="text" id="intials" class="form-control"/>
                              </div>

                           </div>
                           <!-- /Col -->
                           <!-- Col -->
                           <div class="col-md-6">

                              <!-- Form Group -->
                              <div class="form-group">
                                 <label>
                                    First
                                 </label>
                                 <input name="initials" type="text" id="intials" class="form-control"/>
                              </div>
                              <!-- Form Group -->
                              <div class="form-group">
                                 <label>
                                    Middle
                                 </label>
                                    <input name="initials" type="text" id="intials" class="form-control"/>
                              </div>
                              <!-- Form Group -->
                              <div class="form-group">
                                 <label>
                                    Surname
                                 </label>
                                    <input name="initials" type="text" id="intials" class="form-control"/>
                              </div>
                              <!-- Form Group -->
                              <div class="form-group">
                                 <label>
                                    DateOfBirth
                                 </label>
                                    <input name="initials" type="text" id="intials" class="form-control"/>
                              </div>
                              <!-- Form Group -->
                              <div class="form-group">
                                 <label>
                                    Gender <br>
                                 </label>
                                       <input type="radio" name="gender" id="acc_typeg" value="LOCAL" /><label for="acc_typeg">Male</label>
                                       <input id="acc_type_1g" type="radio" name="gender" value="GLOBAL" /><label for="acc_type_1g">Femal</label>
                              </div>

                           </div>
                           <!-- /Col -->
                        </div>
                        <!-- /Row -->	
                        <div class="row">
                           <div class="col-md-4 col-md-offset-4">
                              <ul class="data-list" id="terms">
                                 <li>
                                    <strong>Do you accept <a href="#" data-toggle="modal" data-target="#terms-txt">terms and conditions</a> ?</strong>
                                    <label class="switch-light switch-ios ">
                                    <input type="checkbox" name="terms" class="required fix_ie8" value="yes">
                                    <span>
                                    <span class="ie8_hide">No</span>
                                    <span>Yes</span>
                                    </span>
                                    <a></a>
                                    </label>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <!-- end step -->
                     <div class="step">
                        <!-- Row -->
                        <div class="row">
                           <!-- Col -->
                           <h3>Contact Details</h3>
                      
                           <!-- Col -->
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label>
                                 Address line 1
                                 </label>
                                 <input type="text" class="form-control" placeholder="Enter Name">
                              </div>
                           </div>
                           <!-- /Col -->
                           <!-- Col -->
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label>
                                 Address Line 2
                                 </label>
                                 <input type="text" class="form-control" placeholder="Enter Name">
                              </div>
                           </div>
                           <!-- /Col -->
                           <!-- Col -->
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label>
                                 Country
                                 </label>
                                 <select class="form-control">
                                    <?php include("countries.php") ; ?>
                                 </select>
                              </div>
                           </div>
                           <!-- /Col -->
                           <!-- Col -->
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label>
                                 Phone Number
                                 </label>
                                 <input type="text" class="form-control" placeholder="Enter Name">
                              </div>
                           </div>
                           <!-- /Col -->
                           <!-- Col -->
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label>
                                 Email
                                 </label>
                                 <input type="email" class="form-control" placeholder="Enter Email">
                              </div>
                           </div>
                           <!-- /Col -->
                        </div>

                     </div>
                     <!-- end step-->
                     <div class="step">
                        <h3> Attributes</h3>
                        <div class="row">
                           <div class="col-md-12">
                              <p>
                                 Trading Status
                              </p>
                           </div>
                           <div class="col-md-6">
                              <div class="radio">                              
                                 <label>
                                 <input style  = "margin-top: 0px;" value="" name="acnt-optn" type="radio" checked="">
                                 <strong style  = "font-size:11px;">Dealing Allowed</strong> 
                                 </label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="radio">
                                 <label>
                                 <input style  = "margin-top: 0px;" value="" name="acnt-optn" type="radio">
                                 <strong style  = "font-size:11px;">Trade  Suspended</strong> 
                                 </label>
                              </div>
                           </div>
                        </div>
                        <!-- Row -->
                        <div class="row">
                           <!-- Col -->
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label>
                                 Client Type
                                 </label>
                                 <select class="form-control">
                                    <option>
                                       Select type
                                    </option>
                                    <option>
                                       Admin
                                    </option>
                                    <option>
                                       User
                                    </option>
                                    <option>
                                       client
                                    </option>
                                    <option>
                                       Server
                                    </option>
                                 </select>
                              </div>
                           </div>
                           <!-- /Col -->
                           <!-- Col -->
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label>
                                 &nbsp;
                                 </label>
                                 <input type="text" class="form-control" placeholder="0">
                              </div>
                           </div>
                           <!-- /Col -->
                        </div>
                     </div>
                     <!-- end step-->
                     <div class="step">
                           <h3>Attachements</h3>
                           <!-- Row -->
                           <div class="row">
                              <!-- Col -->
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label>
                                    Document name
                                    </label>
                                    <select class="form-control">
                                       <option>
                                          Select type
                                       </option>
                                       <option>
                                          Admin
                                       </option>
                                       <option>
                                          User
                                       </option>
                                       <option>
                                          client
                                       </option>
                                       <option>
                                          Server
                                       </option>
                                    </select>
                                 </div>
                              </div>
                              <!-- /Col -->
                              <!-- Col -->
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label>
                                    Select Document 
                                    </label>
                                    <input type="file" class="form-control" placeholder="0">
                                 </div>
                              </div>
                              <!-- /Col -->
                           </div>
                     </div>

                     <!-- end step-->
                     <div class="step">
                           <h3>Payment Information</h3>
                           <p>
                              Divindent Payment Mandate
                           </p>
                           <!-- Row -->
                           <div class="row">
                              <!-- Col -->
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label>
                                    Payee
                                    </label>
                                    <input type="text" class="form-control" placeholder="Enter Name">
                                 </div>
                              </div>
                              <!-- /Col -->
                              <!-- Form Group -->
                              <div class="form-group">
                                 <label class="checkbox-inline">
                                 <input style  = "margin-top: 0px;" type="checkbox" value="">
                                 Use account name
                                 </label>
                              </div>
                              <!-- /Form Group -->
                           </div>
                           <hr>
                           <p>
                              Banking details
                           </p>
                           <!-- Row -->
                           <div class="row">
                              <!-- Col -->
                              <div class="col-md-12">
                                 <div class="form-group">
                                    <label>
                                    Account Number
                                    </label>
                                    <input type="text" class="form-control" placeholder="Enter Name">
                                 </div>
                              </div>
                              <!-- /Col -->
                              <!-- Col -->
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label>
                                    Bank Name
                                    </label>
                                    <select class="form-control">
                                       <option>
                                          Select Bank
                                       </option>
                                       <option>
                                          CABS
                                       </option>
                                       <option>
                                          AGRI BANK
                                       </option>
                                       <option>
                                          France
                                       </option>
                                       <option>
                                          Spain
                                       </option>
                                    </select>
                                 </div>
                              </div>
                              <!-- /Col -->
                              <!-- Col -->
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label>
                                    Branch
                                    </label>
                                    <select class="form-control">
                                       <option>
                                          Select branch
                                       </option>
                                       <option>
                                          New York
                                       </option>
                                       <option>
                                          Paris
                                       </option>
                                       <option>
                                          Nairobi
                                       </option>
                                       <option>
                                          Cairo
                                       </option>
                                    </select>
                                 </div>
                              </div>
                              <!-- /Col -->
                           </div>
                           <!-- /Row -->
                           <hr>
                           <p>
                              Mobile Money Account
                           </p>
                           <!-- Row -->
                           <div class="row">
                              <!-- Col -->
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label>
                                    Mobile Money
                                    </label>
                                    <select class="form-control">
                                       <option>
                                          Select 
                                       </option>
                                       <option>
                                          Ecocash
                                       </option>
                                       <option>
                                          Telecash
                                       </option>
                                    </select>
                                 </div>
                              </div>
                              <!-- /Col -->
                              <!-- Col -->
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label>
                                    Mobile Number
                                    </label>
                                    <input type="text" class="form-control" placeholder="Enter Name">
                                 </div>
                              </div>
                              <!-- /Col -->
                           </div>
                           <!-- /Row -->
                     </div>

                     <!-- end step-->
                     <div class="submit step" id="complete">
                        <i class="icon-check"></i>
                        <h3>Registration complete! Thank you for you time.</h3>
                        <button type="submit" name="process" class="submit">Submit the registration details</button>
                        <button type="clear" name="process_cancel" class="submit">Cancel</button>
                     </div>
                     <!-- end submit step -->
                     </div>
                     <!-- end middle-wizard -->
                     <div id="bottom-wizard">
                        <button type="button" name="backward" class="backward">Backward</button>
                        <button type="button" name="forward" class="forward">Forward </button>
                     </div>
                     <!-- end bottom-wizard -->
               </form>
               </div><!-- end Survey container -->
               <div class="row">
                  <div class="col-md-12">
                     <h2>Thank you for your time<span>This help us to improve our service and customer satisfaction.</span></h2>
                  </div>
               </div>
               <!-- end row -->
               <div class="row">
                  <div class="col-md-4 col-sm-4 add_bottom_30 box">
                     <p><img src="img/icon-1.png" alt="Icon"></p>
                     <h3>All you need is</h3>
                     <p>
                        To fill in all the required fields. Get an email with your login details.
                     </p>
                  </div>
                  <div class="col-md-4 col-sm-4 add_bottom_30 box">
                     <p><img src="img/icon-2.png" alt="Icon"></p>
                     <h3>Access your account</h3>
                     <p>
                        Receive an email with your login details and login in to your account and start trading
                     </p>
                  </div>
                  <div class="col-md-4 col-sm-4 add_bottom_30 box">
                     <p><img src="img/icon-3.png" alt="Icon"></p>
                     <h3>Trading</h3>
                     <p>
                        Choose the securities your want to trade , and post a deal note
                     </p>
                  </div>
               </div>
               <!-- end row -->
            </section>
            <!-- end section main container -->
            <footer>
               <section id="footer_2">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-6">
                           <ul id="footer-nav">
                              <li>Copyright © Escrosystem </li>
                              <li><a href="#">Terms of Use</a></li>
                              <li><a href="#">Privacy</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </section>
            </footer>
            <div id="toTop">Back to Top</div>
            <!-- OTHER JS --> 
            <script src="js/jquery.validate.js"></script>
            <script src="js/jquery.placeholder.js"></script>
            <script src="js/jquery.tweet.min.js"></script>
            <script src="js/jquery.bxslider.min.js"></script>
            <script src="js/quantity-bt.js"></script>
            <script src="js/bootstrap.js"></script>
            <script src="js/retina.js"></script>
            <script src="js/owl.carousel.min.js"></script>
            <script src="js/function_s.js"></script>
            <!-- FANCYBOX -->
            <script  src="js/fancybox/source/jquery.fancybox.pack.js?v=2.1.4" type="text/javascript"></script> 
            <script src="js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5" type="text/javascript"></script> 
            <script src="js/fancy_func.js" type="text/javascript"></script> 
         </body>
      </html>