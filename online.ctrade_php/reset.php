<!DOCTYPE html>
    <!--[if IE 9 ]><html class="ie9"><![endif]-->

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C-Trade Online</title>
        
        <!-- Vendor CSS -->
        <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="vendors/bower_components/google-material-color/dist/palette.css" rel="stylesheet">
        <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
            
        <!-- CSS -->
        <link href="css/app.min.1.css" rel="stylesheet">
        <link href="css/app.min.2.css" rel="stylesheet">
    </head>
    
    <body>

        <div class="login" data-lbg="teal">
            <!-- Login -->
            <div class="l-block toggled" id="l-login">
            <form action ="online.ctrade_php/func/forget_pass.php" method="post">
                <div class="lb-body">


                    <h1>Please enter new password below</h1>
                    Email : 
                    <?php 

                    echo @$_GET['email'] ;

                    ?>
                    <input type="hidden" value="<?php echo @$_GET['email'] ;?>" name ="keyz">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            <input type="password" name ="pass" class="input-sm form-control fg-input" placeholder="Enter New Pasword" required>
                        </div>
                    </div>

                    <button class="btn palette-Teal bg" type ="submit">Reset Password</button>

                </div>
            </form>
            </div>

        </div>



        <!-- Javascript Libraries -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>



        <script src="js/functions.js"></script>
        
    </body>

</html>