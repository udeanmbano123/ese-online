

        </section>

        <!-- Page Loader -->
        <div class="page-loader palette-Grey bg">
            <div class="preloader pl-xl pls-white">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20"/>
                </svg>
            </div>
        </div>
        

            <!-- end section main container -->
            <footer>
               <section id="footer_2">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-6">
                           <ul id="footer-nav">
                              <li>Copyright © Escrow Systems </li>
                              <li><a href="#">Terms of Use</a></li>
                              <li><a href="#">Privacy</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </section>
            </footer>
                    
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
        
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        
        <script src="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="vendors/bootstrap-growl/bootstrap-growl.min.js"></script>

        <script src="vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>
        <script src="vendors/summernote/dist/summernote-updated.min.js"></script>


        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
        
        <script src="vendors/bower_components/chosen/chosen.jquery.min.js"></script>
        <script src="vendors/fileinput/fileinput.min.js"></script>
        <script src="vendors/input-mask/input-mask.min.js"></script>
        <script src="vendors/farbtastic/farbtastic.min.js"></script>
        <script src="js/jqClock.min.js"></script>

        <script src="js/functions.js"></script>
        <script src="js/actions.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/dataTables.buttons.min.js"></script>
        <script src="js/buttons.print.min.js"></script>
        <script src="js/demo.js"></script>
        <script src="js/jquery.validate.js"></script>
          <script type="text/javascript">

              $(document).on("click", ".modalDefault", function () {
                   var myBookId = $(this).data('id');
                   // alert(myBookId) ;
                   $("#o_numredu").val( $(this).data('id') );
                   // As pointed out in comments, 
                   // alert($("#o_numredu").val())
                   // it is superfluous to have to manually call the modal.
                   $('#modalDefault').modal('show');
              });

$(document).ready(function(){



                // Wait for the DOM to be ready
                $(function() {
                  // Initialize form validation on the registration form.
                  // It has the name attribute "registration"
                  $("form[name='invoice']").validate({
                    // Specify validation rules
                    rules: {
                      // The key name on the left side is the name attribute
                      // of an input field. Validation rules are defined
                      // on the right side

                      company: "required",
                      instrument: "required",
                      security_dealer: "required",
                      order_type: "required",
                      price: "required",
                      qnty: "required"
                    },
                    // Specify validation error messages
                    messages: {
                      company : "Please select  Company",
                      instrument : "Please select an instrument",
                      security_dealer : "Please select securtiy dealer",
                      order_type : "Please order type",
                      price : "Please enter order price",
                      qnty : "Please enter quantity"
                    },
                    // Make sure the form is submitted to the destination defined
                    // in the "action" attribute of the form when valid
                    submitHandler: function(form) {
                      form.submit();
                    }
                  });
                });


    $('input[name="order_type"]').bind('change',function(){
      // alert("hello") ;
        var showOrHide = ($(this).val() == 'sell') ? true : false;
        // $('#competeyes').toggle(showOrHide);
        if(showOrHide){
          $("#place_order").css({'border-right': 'solid 20px #f68ea5'});
        }else{
          $("#place_order").css({'border-right': 'solid 20px #07381a'});
        }

     });

$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'print'
        ]
    } );
} );


    $("#clock4").clock({"format":"24","calendar":"false"});
      window.setInterval(function(){
        var initialAmmtClose = $('#live_close').text() ;
        var initialAmmtOpen = $('#live_open').text() ;
        var initialAmmtHigh = $('#live_high').text() ;
        var initialAmmtLow = $('#live_low').text() ;
        var initialAVolume= $('#live_volume').text() ;
        var initialATime= $('#live_time').text() ;
        var initialATimeStatus= $('#live_time_status').text() ;
        var ChangeMe = false ; 
        var theAmmt ; 
        var dt ;
        $.get( "nowtime.php", function(data) {
          dt = data ;
          //alert(data);
        });

        // alert("hello" + initialAmmt) ; 
        $.getJSON("requestedjson.php", function(data, status){
          theAmmt = data.close ; 
          if(parseFloat(theAmmt) > parseFloat(initialAmmtClose)) {
                      ChangeMe   = true ; 
            $('#live_close').fadeOut("slow", function(){
              $('#live_close').html('<font color  = "green" >' + theAmmt + '</font>');
              $('#live_close').fadeIn("slow");
            } );
            // $('#load_tweets').load().fadeIn("slow");
          }
          else if(parseFloat(theAmmt) < parseFloat(initialAmmtClose)) {
                      ChangeMe   = true ; 
            $('#live_close').fadeOut("slow", function(){
              $('#live_close').html('<font color  = "red" >' + theAmmt + '</font>');
              $('#live_close').fadeIn("slow");

            } 
            );      
            // $('#load_tweets').load('<font color  = "red" >' + theAmmt + '</font>').fadeIn("slow");
          }


          if(parseFloat(data.volume) != parseFloat(initialAVolume)) {
              $('#live_volume').fadeOut("slow", function(){
                $('#live_volume').html('<font color  = "black" >' + data.volume + '</font>');
                $('#live_volume').fadeIn("slow");
              } );
            }

           if(parseFloat(data.open) != parseFloat(initialAmmtOpen)) {
              $('#live_open').fadeOut("slow", function(){
                $('#live_open').html('<font color  = "black" >' + data.open + '</font>');
                $('#live_open').fadeIn("slow");
              } );
            }
          if(parseFloat(data.high) != parseFloat(initialAmmtHigh)) {
              $('#live_high').fadeOut("slow", function(){
                $('#live_high').html('<font color  = "black" >' + data.high + '</font>');
                $('#live_high').fadeIn("slow");
              } );
            }
          if(parseFloat(data.low) != parseFloat(initialAmmtLow)) {
              $('#live_low').fadeOut("slow", function(){
                $('#live_low').html('<font color  = "black" >' + data.low + '</font>');
                $('#live_low').fadeIn("slow");
              } );
            }


          if(data.times != dt) {
              $('#live_time').fadeOut("slow", function(){
                $('#live_time').html('<font color  = "black" >' + dt + '</font>');
                $('#live_time').fadeIn("slow");
              } );
            }      

            if(data.statustime != initialATimeStatus) {
              $('#live_time_status').fadeOut("slow", function(){
                $('#live_time_status').html('<font color  = "black" >' + data.statustime + '</font>');
                $('#live_time_status').fadeIn("slow");
              } );
            }

          });
      }, 6000);
  
});

  </script>
    </body>
  
</html>