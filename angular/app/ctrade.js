



(function() {
  var __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  angular.module('localytics.directives', []);

  angular.module('localytics.directives').directive('chosen', [
    '$timeout', function($timeout) {
      var CHOSEN_OPTION_WHITELIST, NG_OPTIONS_REGEXP, isEmpty, snakeCase;
      NG_OPTIONS_REGEXP = /^\s*(.*?)(?:\s+as\s+(.*?))?(?:\s+group\s+by\s+(.*))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+(.*?)(?:\s+track\s+by\s+(.*?))?$/;
      CHOSEN_OPTION_WHITELIST = ['noResultsText', 'allowSingleDeselect', 'disableSearchThreshold', 'disableSearch', 'enableSplitWordSearch', 'inheritSelectClasses', 'maxSelectedOptions', 'placeholderTextMultiple', 'placeholderTextSingle', 'searchContains', 'singleBackstrokeDelete', 'displayDisabledOptions', 'displaySelectedOptions', 'width'];
      snakeCase = function(input) {
        return input.replace(/[A-Z]/g, function($1) {
          return "_" + ($1.toLowerCase());
        });
      };
      isEmpty = function(value) {
        var key;
        if (angular.isArray(value)) {
          return value.length === 0;
        } else if (angular.isObject(value)) {
          for (key in value) {
            if (value.hasOwnProperty(key)) {
              return false;
            }
          }
        }
        return true;
      };
      return {
        restrict: 'A',
        require: '?ngModel',
        terminal: true,
        link: function(scope, element, attr, ngModel) {
          var chosen, defaultText, disableWithMessage, empty, initOrUpdate, match, options, origRender, removeEmptyMessage, startLoading, stopLoading, valuesExpr, viewWatch;
          element.addClass('localytics-chosen');
          options = scope.$eval(attr.chosen) || {};
          angular.forEach(attr, function(value, key) {
            if (__indexOf.call(CHOSEN_OPTION_WHITELIST, key) >= 0) {
              return options[snakeCase(key)] = scope.$eval(value);
            }
          });
          startLoading = function() {
            return element.addClass('loading').attr('disabled', true).trigger('chosen:updated');
          };
          stopLoading = function() {
            return element.removeClass('loading').attr('disabled', false).trigger('chosen:updated');
          };
          chosen = null;
          defaultText = null;
          empty = false;
          initOrUpdate = function() {
            if (chosen) {
              return element.trigger('chosen:updated');
            } else {
              chosen = element.chosen(options).data('chosen');
              return defaultText = chosen.default_text;
            }
          };
          removeEmptyMessage = function() {
            empty = false;
            return element.attr('data-placeholder', defaultText);
          };
          disableWithMessage = function() {
            empty = true;
            return element.attr('data-placeholder', chosen.results_none_found).attr('disabled', true).trigger('chosen:updated');
          };
          if (ngModel) {
            origRender = ngModel.$render;
            ngModel.$render = function() {
              origRender();
              return initOrUpdate();
            };
            if (attr.multiple) {
              viewWatch = function() {
                return ngModel.$viewValue;
              };
              scope.$watch(viewWatch, ngModel.$render, true);
            }
          } else {
            initOrUpdate();
          }
          attr.$observe('disabled', function() {
            return element.trigger('chosen:updated');
          });
          if (attr.ngOptions && ngModel) {
            match = attr.ngOptions.match(NG_OPTIONS_REGEXP);
            valuesExpr = match[7];
            scope.$watchCollection(valuesExpr, function(newVal, oldVal) {
              var timer;
              return timer = $timeout(function() {
                if (angular.isUndefined(newVal)) {
                  return startLoading();
                } else {
                  if (empty) {
                    removeEmptyMessage();
                  }
                  stopLoading();
                  if (isEmpty(newVal)) {
                    return disableWithMessage();
                  }
                }
              });
            });
            return scope.$on('$destroy', function(event) {
              if (typeof timer !== "undefined" && timer !== null) {
                return $timeout.cancel(timer);
              }
            });
          }
        }
      };
    }
  ]);

}).call(this);


(function(angular) {

    'use strict';

    angular.module('tabs', []);

    angular.module('tabs')
        .directive('ngTabs', ngTabsDirective);

    function ngTabsDirective() {
        return {
            scope: true,
            restrict: 'EAC',
            controller: ngTabsController
        };
    }

    function ngTabsController($scope) {
        $scope.tabs = {
            index: 0,
            count: 0
        };

        this.headIndex = 0;
        this.bodyIndex = 0;

        this.getTabHeadIndex = function () {
            return $scope.tabs.count = ++this.headIndex;
        };

        this.getTabBodyIndex = function () {
            return ++this.bodyIndex;
        };
    }

    ngTabsController.$inject = ['$scope'];


    angular.module('tabs')
        .directive('ngTabHead', ngTabHeadDirective);

    function ngTabHeadDirective() {
        return {
            scope: false,
            restrict: 'EAC',
            require: '^ngTabs',
            link: function (scope, element, attributes, controller) {
                var index = controller.getTabHeadIndex();
                var value = attributes.ngTabHead;
                var active = /[-*\/%^=!<>&|]/.test(value) ? scope.$eval(value) : !!value;

                scope.tabs.index = scope.tabs.index || ( active ? index : null );

                element.bind('click', function () {
                    scope.tabs.index = index;
                    scope.$$phase || scope.$apply();
                });

                scope.$watch('tabs.index', function () {
                    element.toggleClass('active', scope.tabs.index === index);
                });
            }
        };
    }


    angular.module('tabs')
        .directive('ngTabBody', ngTabBodyDirective);

    function ngTabBodyDirective() {
        return {
            scope: false,
            restrict: 'EAC',
            require: '^ngTabs',
            link: function (scope, element, attributes, controller) {
                var index = controller.getTabBodyIndex();

                scope.$watch('tabs.index', function () {
                    element.toggleClass(attributes.ngTabBody + ' ng-show', scope.tabs.index === index);
                });
            }
        };
    }

})(angular);

angular.module('angular-encryption', [])

.factory("sha256", function() {

  return {

    convertToSHA256: function(data){
      

      var rotateRight = function (n,x) {
        return ((x >>> n) | (x << (32 - n)));
      }
      var choice = function (x,y,z) {
        return ((x & y) ^ (~x & z));
      }
      function majority(x,y,z) {
        return ((x & y) ^ (x & z) ^ (y & z));
      }
      function sha256_Sigma0(x) {
        return (rotateRight(2, x) ^ rotateRight(13, x) ^ rotateRight(22, x));
      }
      function sha256_Sigma1(x) {
        return (rotateRight(6, x) ^ rotateRight(11, x) ^ rotateRight(25, x));
      }
      function sha256_sigma0(x) {
        return (rotateRight(7, x) ^ rotateRight(18, x) ^ (x >>> 3));
      }
      function sha256_sigma1(x) {
        return (rotateRight(17, x) ^ rotateRight(19, x) ^ (x >>> 10));
      }
      function sha256_expand(W, j) {
        return (W[j&0x0f] += sha256_sigma1(W[(j+14)&0x0f]) + W[(j+9)&0x0f] + 
      sha256_sigma0(W[(j+1)&0x0f]));
      }

      /* Hash constant words K: */
      var K256 = new Array(
        0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
        0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
        0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
        0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
        0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
        0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
        0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
        0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
        0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
        0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
        0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
        0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
        0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
        0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
        0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
        0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
      );

      /* global arrays */
      var ihash, count, buffer;
      var sha256_hex_digits = "0123456789abcdef";

      /* Add 32-bit integers with 16-bit operations (bug in some JS-interpreters: 
      overflow) */
      function safe_add(x, y)
      {
        var lsw = (x & 0xffff) + (y & 0xffff);
        var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xffff);
      }

      /* Initialise the SHA256 computation */
      function sha256_init() {
        ihash = new Array(8);
        count = new Array(2);
        buffer = new Array(64);
        count[0] = count[1] = 0;
        ihash[0] = 0x6a09e667;
        ihash[1] = 0xbb67ae85;
        ihash[2] = 0x3c6ef372;
        ihash[3] = 0xa54ff53a;
        ihash[4] = 0x510e527f;
        ihash[5] = 0x9b05688c;
        ihash[6] = 0x1f83d9ab;
        ihash[7] = 0x5be0cd19;
      }

      /* Transform a 512-bit message block */
      function sha256_transform() {
        var a, b, c, d, e, f, g, h, T1, T2;
        var W = new Array(16);

        /* Initialize registers with the previous intermediate value */
        a = ihash[0];
        b = ihash[1];
        c = ihash[2];
        d = ihash[3];
        e = ihash[4];
        f = ihash[5];
        g = ihash[6];
        h = ihash[7];

              /* make 32-bit words */
        for(var i=0; i<16; i++)
          W[i] = ((buffer[(i<<2)+3]) | (buffer[(i<<2)+2] << 8) | (buffer[(i<<2)+1] 
      << 16) | (buffer[i<<2] << 24));

              for(var j=0; j<64; j++) {
          T1 = h + sha256_Sigma1(e) + choice(e, f, g) + K256[j];
          if(j < 16) T1 += W[j];
          else T1 += sha256_expand(W, j);
          T2 = sha256_Sigma0(a) + majority(a, b, c);
          h = g;
          g = f;
          f = e;
          e = safe_add(d, T1);
          d = c;
          c = b;
          b = a;
          a = safe_add(T1, T2);
              }

        /* Compute the current intermediate hash value */
        ihash[0] += a;
        ihash[1] += b;
        ihash[2] += c;
        ihash[3] += d;
        ihash[4] += e;
        ihash[5] += f;
        ihash[6] += g;
        ihash[7] += h;
      }

      /* Read the next chunk of data and update the SHA256 computation */
      function sha256_update(data, inputLen) {
        var i, index, curpos = 0;
        /* Compute number of bytes mod 64 */
        index = ((count[0] >> 3) & 0x3f);
              var remainder = (inputLen & 0x3f);

        /* Update number of bits */
        if ((count[0] += (inputLen << 3)) < (inputLen << 3)) count[1]++;
        count[1] += (inputLen >> 29);

        /* Transform as many times as possible */
        for(i=0; i+63<inputLen; i+=64) {
                      for(var j=index; j<64; j++)
            buffer[j] = data.charCodeAt(curpos++);
          sha256_transform();
          index = 0;
        }

        /* Buffer remaining input */
        for(var j=0; j<remainder; j++)
          buffer[j] = data.charCodeAt(curpos++);
      }

      /* Finish the computation by operations such as padding */
      function sha256_final() {
        var index = ((count[0] >> 3) & 0x3f);
              buffer[index++] = 0x80;
              if(index <= 56) {
          for(var i=index; i<56; i++)
            buffer[i] = 0;
              } else {
          for(var i=index; i<64; i++)
            buffer[i] = 0;
                      sha256_transform();
                      for(var i=0; i<56; i++)
            buffer[i] = 0;
        }
              buffer[56] = (count[1] >>> 24) & 0xff;
              buffer[57] = (count[1] >>> 16) & 0xff;
              buffer[58] = (count[1] >>> 8) & 0xff;
              buffer[59] = count[1] & 0xff;
              buffer[60] = (count[0] >>> 24) & 0xff;
              buffer[61] = (count[0] >>> 16) & 0xff;
              buffer[62] = (count[0] >>> 8) & 0xff;
              buffer[63] = count[0] & 0xff;
              sha256_transform();
      }

      /* Split the internal hash values into an array of bytes */
      function sha256_encode_bytes() {
              var j=0;
              var output = new Array(32);
        for(var i=0; i<8; i++) {
          output[j++] = ((ihash[i] >>> 24) & 0xff);
          output[j++] = ((ihash[i] >>> 16) & 0xff);
          output[j++] = ((ihash[i] >>> 8) & 0xff);
          output[j++] = (ihash[i] & 0xff);
        }
        return output;
      }

      /* Get the internal hash as a hex string */
      function sha256_encode_hex() {
        var output = new String();
        for(var i=0; i<8; i++) {
          for(var j=28; j>=0; j-=4)
            output += sha256_hex_digits.charAt((ihash[i] >>> j) & 0x0f);
        }
        return output;
      }

      

      /* test if the JS-interpreter is working properly */
      function sha256_self_test()
      {
        return sha256_digest("message digest") == 
      "f7846f55cf23e14eebeab5b4e1550cad5b509e3348fbc4efa3a1413d393cb650";
      }

      sha256_init();
      sha256_update(data, data.length);
      sha256_final();
      return sha256_encode_hex();

    }

  }

});


var app = angular.module('myApp', ['ngRoute', 'localytics.directives' ,'angular-encryption','ngResource',  'nvd3ChartDirectives', 'ngSanitize' , 'ui.bootstrap', 'ngAnimate', 'toaster', 'ngDialog' ,'ngStorage', 'nvd3' , 'ngIdle', 'tabs']);


app.config(['$routeProvider','IdleProvider', 'KeepaliveProvider',
  function ($routeProvider,IdleProvider, KeepaliveProvider) {
        IdleProvider.idle(600);
        IdleProvider.timeout(60);
        KeepaliveProvider.interval(60);
        $routeProvider.
        when('/home', {
            title: 'Home',
			cache: false,
            templateUrl: 'templates/home_unlogged.html',
            controller: 'notLoggedCtrl'
        })
        .when('/logout', {
            title: 'Logout',
			cache: false,
            templateUrl: 'partials/login.html',
            controller: 'logoutCtrl'
        })
        .when('/signup', {
            title: 'Signup',
			cache: false,
            templateUrl: 'partials/signup.html',
            controller: 'authCtrl'
        })
        .when('/ipo', {
            title: 'IPO',
			cache: false,
            templateUrl: 'templates/home_ipo_opening.html',
            controller: 'dashboardCtrl'
        })
        .when('/otp', {
            title: 'OTP',
			cache: false,
            templateUrl: 'templates/home_logged_otp.html',
            controller: 'dashboardCtrl'
        })
        .when('/graph', {
            title: 'Graphs',
			cache: false,
            templateUrl: 'templates/home_graphs.html',
            controller: 'dashboardCtrl'
        })
        .when('/graph_one', {
            title: 'Graphs',
			cache: false,
            templateUrl: 'templates/home_graphs_1.html',
            controller: 'dashboardCtrl'
        })

        .when('/graph_two', {
            title: 'Graphs',
			cache: false,
            templateUrl: 'templates/home_graphs_2x1.html',
            controller: 'dashboardCtrl'
        })
        .when('/graph_four', {
            title: 'Graphs',
			cache: false,
            templateUrl: 'templates/home_graphs_2x2.html',
            controller: 'dashboardCtrl'
        })
        .when('/dashboard', {
            title: 'Dashboard',
			cache: false,
            templateUrl: 'templates/home_logged.html',
            controller: 'dashboardCtrl'
            
        })
        .when('/', {
            title: 'Home',
			cache: false,
            templateUrl: 'templates/home_unlogged.html',
            controller: 'notLoggedCtrl',
            role: '0'
        })
        .otherwise({
            redirectTo: '/home'
        });
  }]
  ).run(function ($rootScope, $location,$sessionStorage, Data) {
        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            $rootScope.authenticated = false;
            if($sessionStorage.id != null){
                $rootScope.id = $sessionStorage.id;
                $rootScope.username = $sessionStorage.username;
                $rootScope.email = $sessionStorage.email;
                $rootScope.broker = $sessionStorage.broker;

            }else{
                $location.path("/home");
            }

        });
    });

app.controller('VerifyOTPCtrl', function($scope, $rootScope, $sessionStorage, ngDialog, $location, $http, Data) {
    $scope.userInsPIN = '';
    $scope.userEmailVeri = $sessionStorage.otp ;
    $scope.myFuncVerify = function () {
             if($sessionStorage.otp == document.getElementById('textbox_id_pinveri').value){
              window.location.href="http://trading.ssx.co.sz:88/ctrade/#/dashboard";
              if (window.confirm('Successfully logged in ')) {
                    
                    window.location.reload();   
               }
              else{
                    window.location.reload();   
              }            
                      
        }else{            
            alert("OTP verification error please try again ") ;
            window.location.reload();
        }

    };

});

app.controller('dashboardCtrl', function ($scope, $rootScope,ngDialog, $interval ,$routeParams,$sessionStorage, $location, $http, Data, Idle, Keepalive) {
   
    $scope.login = {};
    $scope.Login_Name = "Guest";
    $scope.countrx = 0 ; 
    $scope.paramValue = $location.search().message;  
    if ($sessionStorage.cds !== "") {
      if($sessionStorage.cds !== undefined){
          $scope.Login_Name = "logged";
      }
      else{
        $scope.Login_Name = "Guest";
      }

    };



    $scope.started = true;

    function closeModals() {
        if ($scope.warning) {
            $scope.warning.close();
            $scope.warning = null;
        }

        if ($scope.timedout) {
            $scope.timedout.close();
            $scope.timedout = null;
        }
    }

    $scope.$on('IdleStart', function() {
        closeModals();

        $scope.warning = ngDialog.open({
            template: 'warning-dialog.html',
            className: 'ngdialog-theme-default ngdialog-theme-custom modal-warning'
        });

    });

    $scope.$on('IdleEnd', function() {
        closeModals();
    });

    $scope.$on('IdleTimeout', function() {
        closeModals();
        $sessionStorage.$reset();
        window.location.reload();
        $location.path('/home');

    });

    $scope.start = function() {
        closeModals();
        Idle.watch();
        $scope.started = true;
    };

    $scope.stop = function() {
        closeModals();
        Idle.unwatch();
        $scope.started = false;
    };

    $scope.fullname = $sessionStorage.username ; 
    $scope.userEmailVeri = $sessionStorage.otp ; 
    $scope.email = $sessionStorage.email ; 
    $scope.cds = $sessionStorage.cds ;
    $scope.min_value_threshold = $sessionStorage.min_value_threshold ;
    $scope.cash_balance  ; 
    $scope.actualcash_balance  ; 
    $scope.v_cash_balance  ; 
    $scope.cash_transactions  ; 
    $scope.my_orders  ; 
    $scope.my_ipo_orders  ; 
    $scope.total_balance;
    $scope.portfolio_balance;
    $scope.pl_balance;
    $scope.companies_listing; 
    $scope.ctrade_title ; 
    $scope.ctrade_forename;
    $scope.ctrade_surname ;
    $scope.ctrade_mobile ;
    $scope.ctrade_country ;
    $scope.ctrade_custodian;
    $scope.ctrade_broker = $sessionStorage.broker ;
    $scope.ctrade_idnumber;
    $scope.ctrade_bank ;
    $scope.ctrade_branch ;
    $scope.ctrade_acc_num; 
    $scope.total = function(one , two) { 
        return parseFloat(one) * parseFloat(two) 
    }

    var updateClock = function() {

        Data.get('CashTrans?cdsNumber='+$scope.cds ).then(function (results) {
            $scope.cash_transactions =  results ;
        });
        Data.get('getCashBalance?cdsNumber='+$scope.cds ).then(function (results) {
            $scope.cash_balance =  results[0].CashBal ;
            $scope.v_cash_balance =  results[0].VirtCashBal ;
            $scope.total_balance =  results[0].totalAccount ;
            $scope.portfolio_balance =  results[0].MyPotValue ;
            $scope.pl_balance =  results[0].MyProfitLoss ;
            $scope.actualcash_balance =  results[0].ActualCashBal ;
        });
        Data.get('GetMyOrders?cdsNumber='+$scope.cds ).then(function (results) {
            $scope.my_orders =  results ;
        });
        Data.get('GetMyIPOOrders?cdsNumber='+$scope.cds ).then(function (results) {
            $scope.my_ipo_orders =  results ;
        });
        Data.get('getMyPortFolioNew?cdsNumber='+$scope.cds ).then(function (results) {
            $scope.my_portfolio =  results ;
        });        
    };

    Data.get('getAllDetails?cdsNumber='+$scope.cds ).then(function (results) {
        $scope.ctrade_title = results[0].Title ; 
        $scope.ctrade_forename = results[0].Forenames ; 
        $scope.ctrade_surname = results[0].Surname ; 
        $scope.ctrade_mobile = results[0].Mobile ; 
        $scope.ctrade_country = results[0].Country ; 
        $scope.ctrade_custodian = results[0].Custodian ; 
        $scope.ctrade_broker = results[0].BrokerCode ;
        $sessionStorage.min_value_threshold = results[0].min_value_threshold ;         
        $scope.ctrade_idnumber = results[0].idnopp ; 
        $scope.ctrade_bank = results[0].Cash_Bank ; 
        $scope.ctrade_branch = results[0].Cash_Branch ; 
        $scope.ctrade_acc_num = results[0].Cash_AccountNo ; 
    });

    var updateClockNow = function() {
        $scope.ctrade_broker = $sessionStorage.broker ;
    };
    $interval(updateClockNow, 10000);
    $interval(updateClock, 60000);

    Data.get('getCashBalance?cdsNumber='+$scope.cds ).then(function (results) {
        $scope.cash_balance =  results[0].CashBal ;
        $scope.v_cash_balance =  results[0].VirtCashBal ;
        $scope.total_balance =  results[0].totalAccount ;
        $scope.portfolio_balance =  results[0].MyPotValue ;
        $scope.actualcash_balance =  results[0].ActualCashBal ;
        $scope.pl_balance =  results[0].MyProfitLoss ;
    });



    Data.get('CashTrans?cdsNumber='+$scope.cds ).then(function (results) {
       $scope.cash_transactions =  results ;
       $scope.predicate = 'id';  
       $scope.reverse = true;  
       $scope.currentPage = 1;  
       $scope.order = function (predicate) {  
         $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;  
         $scope.predicate = predicate;  
       };  
       $scope.totalItems = $scope.cash_transactions.length;  
       $scope.numPerPage = 5;  
         
    });
       $scope.paginate = function (value) {  
         var begin, end, index;  
         begin = ($scope.currentPage - 1) * $scope.numPerPage;  
         end = begin + $scope.numPerPage;  
         index = $scope.cash_transactions.indexOf(value);  
         return (begin <= index && index < end);  
       };        
       $scope.paginatec = function (valuec) {  
         var beginc, endc, indexc;  
         beginc = ($scope.currentPagec - 1) * $scope.numPerPagec;  
         endc = beginc + $scope.numPerPagec;  
         indexc = $scope.companies_listing.indexOf(valuec);  
         return (beginc <= indexc && indexc < endc);  
       };     
       $scope.paginateo = function (valueo) {  
         var begino, endo, indexo;  
         begino = ($scope.currentPageo - 1) * $scope.numPerPageo;  
         endo = begino + $scope.numPerPageo;  
         indexo = $scope.my_orders.indexOf(valueo);  
         return (begino <= indexo && indexo < endo);  
       }; 
    Data.get('GetMyOrders?cdsNumber='+$scope.cds ).then(function (results) {
        $scope.my_orders =  results ;
       $scope.predicateo = 'id';  
       $scope.reverseo = true;  
       $scope.currentPageo = 1;  
       $scope.ordero = function (predicateo) {  
         $scope.reverseo = ($scope.predicateo === predicateo) ? !$scope.reverseo : false;  
         $scope.predicateo = predicateo;  
       };  
       $scope.totalItemso = $scope.my_orders.length;  
       $scope.numPerPageo = 5;          
    });
    Data.get('getIPOISSUES' ).then(function (results) {
         $scope.ipo =  results ;
    });

    Data.get('GetMyIPOOrders?cdsNumber='+$scope.cds ).then(function (results) {
        $scope.my_ipo_orders =  results ;
    });

    Data.get('getMyPortFolioNew?cdsNumber='+$scope.cds ).then(function (results) {
        $scope.my_portfolio =  results ;
    });

    Data.get('GetCompaniesListForGraphAll').then(function (results) {
        $scope.companies_listing =  results ;
       $scope.predicatec = 'Names';  
       $scope.reversec = true;  
       $scope.currentPagec = 1;  
       $scope.orderc = function (predicatec) {  
         $scope.reversec = ($scope.predicatec === predicatec) ? !$scope.reversec : false;  
         $scope.predicatec= predicatec;  
       };  
       $scope.totalItemsc = $scope.companies_listing.length;  
       $scope.numPerPagec = 10;  
    });


    $scope.logout = function () {
        $sessionStorage.$reset();
        alert("User is now logged out") ;
        window.location.reload();
        $location.path('/home');
    }
    $scope.graphs = function () {
        $location.path('/graph');
    }

    $scope.openOrderDiagBUY = function () {
        ngDialog.open({
            template: 'placeOrderDiagBUY',
            className: 'ngdialog-theme-default ngdialog-theme-custom'
        });
    };

    $scope.openOrderDiagBUYIPO = function () {
        ngDialog.open({
            template: 'placeOrderDiagBUYIPO',
            className: 'ngdialog-theme-default ngdialog-theme-custom'
        });
    };

    $scope.openDiagPerfomance = function () {
        ngDialog.open({
            template: 'performanceReport',
            className: 'ngdialog-theme-default ngdialog-theme-custom'            
        });
    };

    $scope.openChangeBroker = function () {
        ngDialog.open({
            template: 'placeOrderDiagChangeBroker',
            className: 'ngdialog-theme-default ngdialog-theme-custom'
        });
    };
    $scope.openOrderDiagSELL = function () {
        ngDialog.open({
            template: 'placeOrderDiagSELL',
            className: 'ngdialog-theme-default ngdialog-theme-custom'
        });
    };
    $scope.placeOrderDiagDeposit = function () {
        ngDialog.open({
            template: 'placeOrderDiagDeposit',
            className: 'ngdialog-theme-default ngdialog-theme-custom'           
        });
    };
    $scope.placeOrderDiagWidthdraw = function () {
        ngDialog.open({
            template: 'placeOrderDiagWidthdraw',
            className: 'ngdialog-theme-default ngdialog-theme-custom'           
        });
    };



});



app.controller('OrderIPOCtrl', function($scope, $rootScope, $sessionStorage, ngDialog, $location, $http, Data) {
    $scope.list = [];

    $scope.brokerz ; 
    Data.get('GetBrokers').then(function (results) {
        $scope.brokerz =  results ;
    });

    $scope.company;
    $scope.security = "BOND";
    $scope.order_type;
    $scope.orderType = "Day";
    $scope.quantity;
    $scope.price;
    $scope.cdsNumber;
    $scope.broker;
    $scope.source = "online" ;
    $scope.cds = $sessionStorage.cds ;
    $scope.doOrder = function (orderTyle) {
        Data.get('OrderPostingIPO?company='+$scope.company+'&security='+$scope.security+'&orderTrans='+orderTyle+'&orderType=Day&quantity='+$scope.quantity+'&price=1.0&cdsNumber='+$scope.cds+'&broker=AKRI&source='+$scope.source ).then(function (results) {
            $scope.company = "";
            $scope.security = "";
            $scope.order_type = "";
            $scope.quantity = "";
            $scope.price = "";
            $scope.cdsNumber = "";
            $scope.broker = "";
            ngDialog.closeAll();
            if(results == "1"){
                alert("Order successfully placed");
            }
            else {
                alert(results);
            }
        });

    };

    });

app.controller('OrderCtrl', function($scope,$filter, $rootScope, $sessionStorage, ngDialog, $location, $http, Data) {
    $scope.list = [];
    $scope.my_broker_name ;
    $scope.broker_set ;
    $scope.brokerz ; 
    $scope.marko ; 
    $scope.oq ; 
    $scope.exchange_selected ; 
    $scope.my_style_now = "display:none;" ;  
    $scope.dategtd = new Date() ; 
    $scope.today_ = new Date()
    $scope.isDisabled_sell = false;
    $scope.isDisabled_buy = false;

    $scope.searchButtonText_buy  = "Place Order ► "; 
    $scope.searchButtonText_sell  = "Place Order ► "; 
    if( $sessionStorage.broker != '') {

    }else{
       
        Data.get('GetBrokers').then(function (results) {
            $scope.brokerz =  results ;
        });
    }


    $scope.macompanies_listing; 

    $scope.updatePrice = function () {
        $scope.price = parseFloat($scope.company.split(',')[1]);
        $scope.total =  $scope.quantity *  $scope.price ; 
        $scope.charges_buy = $scope.total * 0.01693 ; 
        $scope.charges_sell = $scope.total * 0.02443 ;         
    };
    
    $scope.updateMarketOrder = function () {
        alert("Hello" +$scope.marko ) ;
    };

    $scope.exchange_selected = 'ESE' ; 
    Data.get('GetCompaniesandPricesEXCHANGE?exchange=ESE').then(function (results) {
        $scope.macompanies_listing =  results ;
    });

    $scope.updateTIF = function () {
      if($scope.tif == "Good Till Day (GTD)"){
        $scope.my_style_now = "style= 'display:true;'" ; 

      }else{
        $scope.my_style_now = "display:none;" ; 
      }
    };

    $scope.test = function () {
        $scope.price = parseFloat($scope.company.split(',')[1]);
        $scope.total =  $scope.quantity *  $scope.price ; 
        $scope.charges_buy = $scope.total * 0.01693 ; 
        $scope.charges_sell = $scope.total *  0.02443 ;    
    };

    $scope.company;
    $scope.security = "Equity";
    $scope.order_type;
    $scope.tif = "Good Till Cancelled (GTC)";
    $scope.orderType = "Day";
    $scope.quantity = 0;
    $scope.total_ammount  ;
    $scope.price ;
    $scope.cdsNumber;
    $scope.gtd_date;
    
    $scope.source = "online" ;
    $scope.cds = $sessionStorage.cds ;
    $scope.brokerThere = $sessionStorage.broker ;
    $scope.tinashe_my_style = "" ; 
    $scope.cancelOrder = function (id , o_type) {
              if (window.confirm('Are you sure you want to cancel order ')) {
                  Data.get('UpdateCancelOrder?cdsnumber='+ $sessionStorage.cds +'&ordernumber='+id +'&orderType='+o_type).then(function (results) {
                      alert(results) ; 
                  });
               }
              else{
                ;
              }     

    };
    
    if($scope.brokerThere == ''){
          ;       
    }else{
        $scope.broker_set = $sessionStorage.broker ; 
        $scope.tinashe_my_style = "display:none;" ;
    }

    

    $scope.doOrder = function (orderTyle) {

        $scope.isDisabled_buy = true;
        $scope.isDisabled_sell = true;
        $scope.searchButtonText_buy = "Please Wait"; 
        $scope.searchButtonText_sell = "Please Wait"; 
        $scope.broker_threshold = $sessionStorage.min_value_threshold ; 
        if($scope.company == null ){
                alert("Please select company " );
                    $scope.searchButtonText_buy  = "Place Order ► "; 
                    $scope.searchButtonText_sell  = "Place Order ► ";    
                    $scope.isDisabled_buy = false;
                    $scope.isDisabled_sell = false;                      
        }     
        if($scope.brokerThere == ''){
            if($scope.my_broker_name != null) {
                $scope.broker_set = $scope.my_broker_name.Code ; 
                $scope.broker_threshold = $scope.my_broker_name.threshold ; 
            }else{
                $scope.broker_threshold = $sessionStorage.min_value_threshold ;

            }
        }

        var upperPrice = $scope.company.split(',')[1] * 1.1;
        var lowerPrice = $scope.company.split(',')[1] * 0.9;
        var upperPrice_zse = $scope.company.split(',')[1] * 1.2;
        var lowerPrice_zse = $scope.company.split(',')[1] * 0.8;
        var upperPrice_zsenow = $scope.company.split(',')[1] * 2 ;
        var company_price_default  = $scope.company.split(',')[1]  ;

        
       
        if($scope.exchange_selected == "ESE" && $scope.price >= upperPrice || $scope.price <= lowerPrice){
            alert("Please enter a price that is within the Price Protection range of +/-10% of the current price " );
            $scope.searchButtonText_buy  = "Place Order ► "; 
            $scope.searchButtonText_sell  = "Place Order ► ";    
            $scope.isDisabled_buy = false;
            $scope.isDisabled_sell = false;                            
        }else if($scope.exchange_selected == "ZSE" && company_price_default >= 0.01 && ($scope.price >= upperPrice_zse || $scope.price <= lowerPrice_zse) ){
            alert("Please enter a price that is within the Price Protection range of +/-20% of the current price " );
            $scope.searchButtonText_buy  = "Place Order ► "; 
            $scope.searchButtonText_sell  = "Place Order ► ";    
            $scope.isDisabled_buy = false;
            $scope.isDisabled_sell = false;    
        }else if($scope.exchange_selected == "ZSE" && company_price_default < 0.01 && $scope.price >= upperPrice_zsenow){
            alert("Please enter a price that is within the Price Protection range of +100% of the current price " );
            $scope.searchButtonText_buy  = "Place Order ► "; 
            $scope.searchButtonText_sell  = "Place Order ► ";    
            $scope.isDisabled_buy = false;
            $scope.isDisabled_sell = false;     
        } else if($scope.price == 0 || $scope.price == null || $scope.price == undefined || $scope.price == "" ||    $scope.price.length == 0){
                alert("Please enter correct price " );
                    $scope.searchButtonText_buy  = "Place Order ► "; 
                    $scope.searchButtonText_sell  = "Place Order ► ";    
                    $scope.isDisabled_buy = false;
                    $scope.isDisabled_sell = false;                              
        }else if($scope.quantity == 0 || $scope.quantity == null || $scope.quantity == undefined || $scope.quantity == "" ||    $scope.quantity.length == 0){
                alert("Please enter correct quantity " );
                    $scope.searchButtonText_buy  = "Place Order ► "; 
                    $scope.searchButtonText_sell  = "Place Order ► ";    
                    $scope.isDisabled_buy = false;
                    $scope.isDisabled_sell = false;                                  
        } else if( ($scope.quantity * $scope.price * 1.01693 ) < $scope.broker_threshold ){
                alert("Your selected broker only accepts orders above $"+$scope.broker_threshold +". Please select another broker and try again." );
                    $scope.searchButtonText_buy  = "Place Order ► "; 
                    $scope.searchButtonText_sell  = "Place Order ► ";    
                    $scope.isDisabled_buy = false;
                    $scope.isDisabled_sell = false;                                  
        } 
          else{
          if ($scope.tif == 'Good Till Day (GTD)'){

                Data.get('OrderPostingMakeNew?company='+$scope.company.split(',')[0]+'&security='+$scope.security+'&tif='+$scope.tif+'&date_='+$filter('date')($scope.dategtd, "dd/MM/yyyy")+'&orderTrans='+orderTyle+'&orderType=Day&quantity='+$scope.quantity+'&price='+$scope.price+'&cdsNumber='+$scope.cds+'&broker='+$scope.broker_set+'&source='+$scope.source ).then(function (results) {

                $scope.company = ",0.0";
                $scope.order_type = "";
                $scope.quantity = "";
                $scope.price = "";
                $scope.cdsNumber = "";
                $scope.broker_set = "";
                ngDialog.closeAll();
                if(results == "1"){
                    $scope.searchButtonText_buy  = "Place Order ► "; 
                    $scope.searchButtonText_sell  = "Place Order ► ";    
                    $scope.isDisabled_buy = false;
                    $scope.isDisabled_sell = false;                
                    alert("Order successfully placed");
                }
                else {
                    $scope.isDisabled_buy = false;
                    $scope.isDisabled_sell = false;    
                    $scope.searchButtonText_buy  = "Place Order ► "; 
                    $scope.searchButtonText_sell  = "Place Order ► ";                     
                    alert(results);
                }
            });
          }else{
                $scope.datiredunhasi = new Date() ;
                $scope.url_send_dt = 'OrderPostingMakeNew?company='+$scope.company.split(',')[0]+'&security='+$scope.security+'&tif='+$scope.tif+'&orderTrans='+orderTyle+'&orderType=Day&quantity='+$scope.quantity+'&price='+$scope.price+'&cdsNumber='+$scope.cds+'&broker='+$scope.broker_set+'&source='+$scope.source + '&date_='+$filter('date')($scope.datiredunhasi, "dd/MM/yyyy");
                Data.get($scope.url_send_dt).then(function (results) {
                $scope.company = ",0.0";
                $scope.order_type = "";
                $scope.quantity = "";
                $scope.price = "";
                $scope.cdsNumber = "";
                $scope.broker_set = "";
                ngDialog.closeAll();
                if(results == "1"){
                    $scope.searchButtonText_buy  = "Place Order ► "; 
                    $scope.searchButtonText_sell  = "Place Order ► ";    
                    $scope.isDisabled_buy = false;
                    $scope.isDisabled_sell = false;                
                    alert("Order successfully placed");
                }
                else {
                    $scope.isDisabled_buy = false;
                    $scope.isDisabled_sell = false;    
                    $scope.searchButtonText_buy  = "Place Order ► "; 
                    $scope.searchButtonText_sell  = "Place Order ► ";                     
                    alert(results);
                }
            });
          }

        }

    };

});


app.controller('notLoggedCtrl', function ($scope, $interval, $rootScope, ngDialog, $location, $http, Data ,$timeout) {
    $scope.home = {};
    $scope.initbalance = "0.00";

    $scope.openDiag = function () {
        ngDialog.open({
            template: 'firstDialog',
            className: 'ngdialog-theme-default ngdialog-theme-custom'         
        });
    };

    $scope.checkPrice = function (price) {
        alert(price ) ;

    };

    $scope.openDiagForget = function () {
        ngDialog.open({
            template: 'forgetPassDialog',
            className: 'ngdialog-theme-default ngdialog-theme-custom'         
        });
    };


    Data.get('MarketWatchbidoffer').then(function (results) {
           $scope.marketwatch = results ;       
    });

    Data.get('MarketWatchZSE').then(function (results) {
            $scope.marketwatchzse = results ; 
           $scope.predicatemez = 'id';  
           $scope.reversemez = true;  
           $scope.currentPagemez = 1;  
           $scope.ordermez = function (predicatemez) {  
             $scope.reversemez = ($scope.predicatemez === predicatemez) ? !$scope.reversemez : false;  
             $scope.predicatemez = predicatemez;  
           };  
           $scope.totalItemsmez = $scope.marketwatchzse.length;  
           $scope.numPerPagemez = 10;      

    });
    
    $scope.paginatemez = function (valuemez) {  
     var beginmez, endmez, indexmez;  
     beginmez = ($scope.currentPagemez - 1) * $scope.numPerPagemez;  
     endmez = beginmez + $scope.numPerPagemez;  
     indexmez = $scope.marketwatchzse.indexOf(valuemez);  
     return (beginmez <= indexmez && indexmez < endmez);  
    };   

    var updateClockzz = function() {
       Data.get('MarketWatchbidoffer').then(function (results) {
                $scope.marketwatch = results ; 
        });
       Data.get('MarketWatchZSE').then(function (results) {
                $scope.marketwatchzse = results ; 
        });
    };
    $scope.graph_company = "" ;
    $scope.graph_company_ZSE = "" ;

    $interval(updateClockzz, 60000);

    Data.get('getMarketCap').then(function (results) {
        $scope.boxes = results ; 
    });


        $scope.moving = false;

        $scope.moveLeft = function() {
            $scope.moving = true;
            $timeout($scope.switchFirst, 1000);
        };
        $scope.switchFirst = function() {
            $scope.boxes.push($scope.boxes.shift());
            $scope.moving = false;
            $scope.$apply();
        };

        $interval($scope.moveLeft, 2000);


    $scope.tableRowClicked=function(data){
        if($scope.graph_company_ZSE != data){
            $scope.graph_company_ZSE = "Loading data ....." ; 
            Data.getn('online.ctrade_php/getPrices.php?company='+data).then(function (results) {
                $scope.graph_company_ZSE = data ; 
                $scope.data_zse = results ; 
                if($scope.rc.api == undefined){
                   ;
                }else{
                    $scope.rc.api.refresh();
                }

            });
        }else{
            alert("Already Loaded graph") ;
        }

    }

    $scope.options = {
        chart: {
            key:'Prices',
            type: 'lineChart',
            height: 250,
            margin : {
                top: 10,
                right: 5,
                bottom: 50,
                left: 40
            },
            x: function(d){return d[0];},
            y: function(d){return d[1];},              
            useVoronoi: false,
            clipEdge: true,
            duration: 100,
            useInteractiveGuideline: true,
            xAxis: {
                showMaxMin: false,                    
                tickFormat: function(d) {
                    return d3.time.format('%x')(new Date(d))
                }
            },
            yAxis: {
                showMaxMin: true,                                        
                tickFormat: function(d){
                    return d3.format(',.4f')(d);
                }
            },
            zoom: {
                enabled: true,
                scaleExtent: [1, 10],
                useFixedDomain: false,
                useNiceScale: false,
                horizontalOff: false,
                verticalOff: true,
                unzoomEventType: 'dblclick.zoom'
            }
        }
    };

    $scope.options1 = {
        chart: {
            type: 'lineChart',
            height: 150,
            margin : {
                top: 10,
                right: 5,
                bottom: 50,
                left: 40
            },
            x: function(d){return d[0];},
            y: function(d){return d[1];},              
            useVoronoi: false,
            clipEdge: true,
            duration: 100,
            useInteractiveGuideline: true,
            xAxis: {
                showMaxMin: false,                    
                tickFormat: function(d) {
                    return d3.time.format(' %x')(new Date(d))
                }
            },
            yAxis: {
                showMaxMin: true,                                        
                tickFormat: function(d){
                    return d3.format(',.4f')(d);
                }
            },
            zoom: {
                enabled: true,
                scaleExtent: [1, 10],
                useFixedDomain: false,
                useNiceScale: false,
                horizontalOff: false,
                verticalOff: true,
                unzoomEventType: 'dblclick.zoom'
            }

        }
    };

    $scope.rc = {} ;

    $scope.getDataServer = function(companyName) { 
        
        $scope.graph_company = companyName ; 
        Data.getn('online.ctrade_php/getPrices.php?company='+companyName).then(function (results) {
            alert("Data Loaded for " + companyName) ; 
            $scope.data = results ; 
            if($scope.rc.api == undefined){
                ;
            }else{
                $scope.rc.api.refresh();
            }

        });


    };

    Data.get('GetCompaniesListForGraphAll').then(function (results) {
        $scope.companies_listing =  results ;
       $scope.predicatec = 'Names';  
       $scope.reversec = true;  
       $scope.currentPagec = 1;  
       $scope.orderc = function (predicatec) {  
         $scope.reversec = ($scope.predicatec === predicatec) ? !$scope.reversec : false;  
         $scope.predicatec= predicatec;  
       };  
       $scope.totalItemsc = $scope.companies_listing.length;  
       $scope.numPerPagec = 10;  
    });

       $scope.paginatec = function (valuec) {  
         var beginc, endc, indexc;  
         beginc = ($scope.currentPagec - 1) * $scope.numPerPagec;  
         endc = beginc + $scope.numPerPagec;  
         indexc = $scope.companies_listing.indexOf(valuec);  
         return (beginc <= indexc && indexc < endc);  
       };     


        Data.getn('online.ctrade_php/getPrices.php?company=OMZIL').then(function (results) {
                $scope.data = results ; 
        });
        Data.getn('online.ctrade_php/getPrices.php?company=OMZIL').then(function (results) {
                $scope.data_zse = results ; 
        });

        $scope.run = true;
});


app.controller('LoginCtrl', function($scope, $rootScope, $sessionStorage, ngDialog, $location, $http, Data , sha256 ) {
    $scope.list = [];
    $scope.any ;
    $scope.cds_email ;
    $scope.cds_or_email ;
    $scope.resemailsend ;
    $scope.searchButtonText = "NEXT ► " ;
    $scope.inputType = 'text';
    $scope.setReg = 'false' ; 
    $scope.setRegConfig = 'false' ; 
    $scope.doLogin = function (any) { 
        $scope.test = "true";
        $scope.searchButtonText = "Please Wait";       
        myElsz = angular.element(document.querySelector('#cds_email_atp'));
        my_emailz = $scope.any
        if($scope.inputType == "password") {   
            if($scope.setReg == "true"){
                url = 'NewLoginEncry?username='+myElsz.text().replace("CDS/ATP/EMAIL : " , "")+'&password='+sha256.convertToSHA256($scope.any) ; 
                $scope.setReg = "false";
            }else{
                url = 'Registration?username='+myElsz.text().replace("CDS/ATP/EMAIL : " , "")+'&email='+myElsz.text().replace("CDS/ATP/EMAIL : " , "")+'&password='+$scope.any ; 
                $scope.setRegConfig = "true" ; 
            }

        }else{
          url = 'CheckWhoRegisteredCtrade?any='+$scope.any;
        }

        Data.get(url, {
            any: any
        }).then(function (results) {
            $scope.searchButtonText = "NEXT ► ";
            if($scope.inputType == "text") {
                if(results == 1){
                    $scope.cds_email = "CDS/ATP/EMAIL : " + my_emailz ; 
                    myEl = angular.element(document.querySelector('#addr_yangu'));
                    myEl.html('<b>Enter Password</b>');
                    $scope.inputType = 'password';
                    $scope.setReg = "true";
                }
                else if(results == 2){
                    $scope.cds_email = "CDS/ATP/EMAIL : " + my_emailz ; 
                    myEl = angular.element(document.querySelector('#addr_yangu'));
                    myEl.html('<b>Enter your desired password</b>');
                    $scope.inputType = 'password';                    
                    scope.setReg = 'false' ;
                }else if(results == 3){
                    alert("Account is not yet activated please try again later") ; 
                }
                else{
                    alert("Account is not found please Register first") ; 
                }
            }
            else{
                var avll = Math.floor(1000 + Math.random() * 9000);
                $rootScope.authenticated = true;
                $sessionStorage.id = results[0].id ;
                $sessionStorage.username = results[0].username ;
                $sessionStorage.email =results[0].email  ;            
                $sessionStorage.cds =results[0].cds  ;  
                $sessionStorage.min_value_threshold =results[0].min_value_threshold  ;  
                $sessionStorage.broker =results[0].broker  ;  
                $sessionStorage.otp =avll  ;  
                $scope.fullname = $sessionStorage.username ; 
                $scope.userEmailVeri = $sessionStorage.username ; 
                ngDialog.close('ngdialog1');
                if (typeof  $sessionStorage.cds !== "undefined" ) {
                        alert("OTP has been send to your email, Verify your OTP to start trading" ) ;
                        Data.getn('online.ctrade_php/func/send_otp.php?email='+$sessionStorage.email+'&message='+avll).then(function (results) {
                          $scope.resemailsend =  results ;
                        });                             
                                     
                        $location.path('/otp');
                }else if(results == 7){
                    alert("Invalid password please try again") ; 
                }else if($scope.setRegConfig == "true"){
                    alert("New account have been successfully created") ;
                }else{
                    alert("Error occured please contact administrator") ;
                }


            }
        });
        if ($scope.any) {
          $scope.list.push(this.any);
          $scope.any = '';
        }   
    };

    $scope.my_email ; 
    $scope.doForgetPass = function (any) {    

            Data.getn('online.ctrade_php/func/forgetpass_.php?email='+$scope.my_email).then(function (results) {
                $scope.resemailsend =  results ;
                alert("An email has been send follow the reset password link ") ;
            });     

         
          ngDialog.closeAll();
    };

});


app.directive('focus', function() {
    return function(scope, element) {
        element[0].focus();
    }      
});


angular.module('blink', [])
.directive('blink', ['$interval', function($interval) {
  return function(scope, element, attrs) {
      var timeoutId;
      
      var blink = function() {
        element.css('visibility') === 'hidden' ? element.css('visibility', 'inherit') : element.css('visibility', 'hidden');
      }
      
      timeoutId = $interval(function() {
        blink();
      }, 1000);
    
      element.css({
        'display': 'inline-block'
      });
      
      element.on('$destroy', function() {
        $interval.cancel(timeoutId);
      });
    };
}]);

app.directive('passwordMatch', [function () {
    return {
        restrict: 'A',
        scope:true,
        require: 'ngModel',
        link: function (scope, elem , attrs,control) {
            var checker = function () {
 

                var e1 = scope.$eval(attrs.ngModel); 

                var e2 = scope.$eval(attrs.passwordMatch);
                if(e2!=null)
                return e1 == e2;
            };
            scope.$watch(checker, function (n) {

                control.$setValidity("passwordNoMatch", n);
            });
        }
    };
}]);

app.directive('extendedLineChart', function(){
    "use strict";
   return {
       restrict: 'E',
       require: '^nvd3SparklineChart',
       link: function($scope, $element, $attributes, nvd3SparklineChart) {
           $scope.d3Call = function(data, chart){

               var svg = d3.select('#' + $scope.id + ' svg')
                   .datum(data);

               var path = svg.selectAll('path');
                    path.data(data)
                    .transition()
                    .ease("linear")
                    .duration(500)

               return svg.transition()
                   .duration(500)
                   .call(chart);

           }
       }
   }


});


app.controller('DepositCtrl', function($scope, $resource , $rootScope, $sessionStorage, ngDialog, $location, $http, Data , $window) {
    $scope.list = [];
    $scope.searchButtonText_dep = " Make a Widthdrawal ► " ;
    $scope.pay_methd;
    $scope.pay_ammt;
    $scope.my_style_phone = "display:none;" ;  
    $scope.my_style_phone_otp = "display:none;" ; 
    $scope.phone_numbr_otp ;  
    $scope.phone_numbr ;  
    $scope.updatePayments = function () {
          
          if($scope.pay_methd =="MTN"){
                $scope.my_style_phone = "style= 'display:true;'" ; 
                $scope.my_style_phone_otp = "display:none;" ;
          }else  if($scope.pay_methd =="SWAZI"){
                $scope.my_style_phone = "style= 'display:true;'" ; 
                $scope.my_style_phone_otp = "display:none;" ;
          }else{
                $scope.my_style_phone = "display:none;" ; 
                $scope.my_style_phone_otp = "display:none;" ; 
          }
    };

    $scope.depositMoney = function () {

        if($scope.pay_ammt == 0 || $scope.pay_ammt == null || $scope.pay_ammt < 10 || $scope.pay_ammt == undefined || $scope.pay_ammt == "" ||    $scope.pay_ammt.length == 0){
                alert("Please enter correct amount");                           
        }else{     
         if($scope.pay_methd =="MTN"){
            
			  //Call MTN api
              $http.get("http://trading.ssx.co.sz/MTNPAY/Payment/"+$scope.phone_numbr+"/"+$scope.pay_ammt).then(function (results) {
                  alert("Deposit request being processed.") ;
                  ngDialog.closeAll();
              });
          }else  if($scope.pay_methd =="SWAZI"){
             
			  //Call SWAZI api
			  $http.get("http://trading.ssx.co.sz/SwaziApp/DebitRequest/"+$scope.phone_numbr+"/"+$scope.pay_ammt).then(function (results) {
                  alert("Deposit request being processed.") ;
                  ngDialog.closeAll();
              });
          }
        }
        

    };

    $scope.brokercz ; 
    Data.get('GetBrokers').then(function (results) {
        $scope.brokercz =  results ;
    });

    $scope.widthdrawMoney = function () {
        $scope.test = "true";
        $scope.searchButtonText = "Please Wait"; 
        if($scope.pay_ammt_with == 0 || $scope.pay_ammt_with == null || $scope.pay_ammt_with == undefined || $scope.pay_ammt_with == "" ||    $scope.pay_ammt_with.length == 0){
            alert("Please enter correct amount");                           
        }else{        
            Data.get('Widthdraw?cdsNumber='+ $sessionStorage.cds +'&ammount='+ $scope.pay_ammt_with).then(function (results) {
                alert(results) ; 
            });
        }


        ngDialog.closeAll();

    };
   
    $scope.changeBroker = function () {
      $scope.change_broker = $scope.my_broker_name.Code ; 
      $scope.broker_threshold = $scope.my_broker_name.threshold ; 

        Data.get('UpdateBroker?cdsnumber='+ $sessionStorage.cds +'&broker='+ $scope.change_broker).then(function (results) {
            alert(results) ; 
            $sessionStorage.broker = $scope.change_broker ;
            $sessionStorage.min_value_threshold =  $scope.broker_threshold ;
        });
        ngDialog.closeAll();

    };


});

app.factory("Data", ['$http', 'toaster',
    function ($http, toaster) { 

        var serviceBase = '//trading.ssx.co.sz/CTRADEAPI/Subscriber/';
        var serviceBase2 = '//ctrade.co.zw/ussdapi/';

        var obj = {};
        obj.toast = function (data) {
            toaster.pop(data.status, "", data.message, 10000, 'trustedHtml');
        }
        obj.get = function (q) {
            return $http.get(serviceBase + q).then(function (results) {
                return results.data;
            });
        };
       obj.getn = function (q) {
            return $http.get(q).then(function (results) {
                return results.data;
            });
        };
        obj.getussd = function (q) {
            return $http.get(serviceBase2 + q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(serviceBase + q, object).then(function (results) {
                return results.data;
            });
        };
        obj.postn = function (q, object) {
            return $http.post(q, object).then(function (results) {
                return results.data;
            });
        };        
        obj.put = function (q, object) {
            return $http.put(serviceBase + q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(serviceBase + q).then(function (results) {
                return results.data;
            });
        };

        return obj;
}]);





app.controller('authCtrl', function ($scope, $rootScope, $routeParams, $location, $http, Data) {
    $scope.login = {};
    $scope.signup = {};
    $scope.doLogin = function (customer) {
        Data.post('login', {
            customer: customer
        }).then(function (results) {
            Data.toast(results);
            alert(results) ;
            if (results.status == "success") {
                $location.path('dashboard');
            }
        });
    };
    $scope.signup = {email:'',password:'',name:'',phone:'',address:''};
    $scope.signUp = function (customer) {
        Data.post('signUp', {
            customer: customer
        }).then(function (results) {
            Data.toast(results);
            if (results.status == "success") {
                $location.path('dashboard');
            }
        });
    };
    
    $scope.persons = [{id: 1, name: 'Alex'}, {id: 2, name: 'David'}];

    $scope.logout = function () {
        $sessionStorage.id ="" ;
        $sessionStorage.username = "" ;
        $sessionStorage.email =""  ;            
        $sessionStorage.cds =""  ;  
        $sessionStorage.min_value_threshold = "" ; 
    }
});


app.controller('AppCtrl', function($scope, $mdDialog) {
  $scope.status = '  ';
  $scope.customFullscreen = false;

  $scope.showAlert = function(ev) {

    $mdDialog.show(
      $mdDialog.alert()
        .parent(angular.element(document.querySelector('#popupContainer')))
        .clickOutsideToClose(true)
        .title('This is an alert title')
        .textContent('You can specify some description text in here.')
        .ariaLabel('Alert Dialog Demo')
        .ok('Got it!')
        .targetEvent(ev)
    );
  };

  $scope.showConfirm = function(ev) {
    var confirm = $mdDialog.confirm()
          .title('Would you like to delete your debt?')
          .textContent('All of the banks have agreed to forgive you your debts.')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Please do it!')
          .cancel('Sounds like a scam');

    $mdDialog.show(confirm).then(function() {
      $scope.status = 'You decided to get rid of your debt.';
    }, function() {
      $scope.status = 'You decided to keep your debt.';
    });
  };

  $scope.showPrompt = function(ev) {
    var confirm = $mdDialog.prompt()
      .title('What would you name your dog?')
      .textContent('Bowser is a common name.')
      .placeholder('Dog name')
      .ariaLabel('Dog name')
      .initialValue('Buddy')
      .targetEvent(ev)
      .required(true)
      .ok('Okay!')
      .cancel('I\'m a cat person');

    $mdDialog.show(confirm).then(function(result) {
      $scope.status = 'You decided to name your dog ' + result + '.';
    }, function() {
      $scope.status = 'You didn\'t name your dog.';
    });
  };

  $scope.showAdvanced = function(ev) {
    $mdDialog.show({
      controller: DialogController,
      templateUrl: 'dialog1.tmpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });
  };

  $scope.showTabDialog = function(ev) {
    $mdDialog.show({
      controller: DialogController,
      templateUrl: 'tabDialog.tmpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
        .then(function(answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function() {
          $scope.status = 'You cancelled the dialog.';
        });
  };

  $scope.showPrerenderedDialog = function(ev) {
    $mdDialog.show({
      contentElement: '#myDialog',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: true
    });
  };

  function DialogController($scope, $mdDialog) {
    $scope.hide = function() {
      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
      $mdDialog.hide(answer);
    };
  }
});