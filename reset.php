<?php
//$email_add = filter_input(INPUT_GET, 'email', FILTER_SANITIZE_URL) ;
$email_add =  htmlspecialchars(@$_GET['email'], ENT_QUOTES, 'UTF-8');
function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

$encrypted_txt = encrypt_decrypt('decrypt', $email_add);

?>
<!DOCTYPE html>
    <!--[if IE 9 ]><html class="ie9"><![endif]-->

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C-Trade Online</title>
        
        <!-- Vendor CSS -->
        <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="vendors/bower_components/google-material-color/dist/palette.css" rel="stylesheet">
        <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link rel="icon" href="https://ctrade.co.zw/wp-content/uploads/2018/02/favicon-150x150.png" sizes="32x32" />
        <link rel="icon" href="https://ctrade.co.zw/wp-content/uploads/2018/02/favicon.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="https://ctrade.co.zw/wp-content/uploads/2018/02/favicon.png" />
        <!-- CSS -->
        <link href="css/app.min.1.css" rel="stylesheet">
        <link href="css/app.min.2.css" rel="stylesheet">
    </head>
    
    <body>

        <div class="login" data-lbg="teal">
            <!-- Login -->
            <div class="l-block toggled" id="l-login">
            <form action ="online.ctrade_php/func/forget_pass.php" method="post">
                <div class="lb-body">


                    <h1>Please enter new password below</h1>
                    Email : 
                    <?php 
                    echo $encrypted_txt ;
                    ?>
                    <input type="hidden" value="<?php echo $email_add ;?>" name ="keyz">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            <input type="password" name ="pass" class="input-sm form-control fg-input" placeholder="Enter New Pasword" required>
                        </div>
                    </div>

                    <button class="btn palette-Teal bg" type ="submit">Reset Password</button>

                </div>
            </form>
            </div>

        </div>



        <!-- Javascript Libraries -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>



        <script src="js/functions.js"></script>
        
    </body>

</html>