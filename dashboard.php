<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<html lang="en" ng-app="myApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C-Trade Online</title>

        <!-- Vendor CSS -->
        <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
        <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">        
        <!-- CSS -->
        <link href="css/app_1.min.css" rel="stylesheet">
        <link href="css/app_2.min.css" rel="stylesheet">
        <link href="css/nv.d3.css" rel="stylesheet">
        <link href="css/bootstrap-table-expandable.css" rel="stylesheet">
        <link rel="stylesheet" href="vendors/ngDialog/css/ngDialog.min.css">
        <link rel="stylesheet" href="vendors/ngDialog/css/ngDialog-theme-default.min.css">
    </head>

    <body  ng-cloak="">
        <header id="header" class="clearfix">
            <ul class="h-inner">
                <li class="hidden-xs">
                    <a href="index.html" class="p-l-10">
                        <img src="img/demo/logo.png" style="width: 34%;" alt="">
                    </a>
                </li>


                <li class="pull-right">
                    <ul class="hi-menu">


                        <li class="dropdown" >
                            <a data-toggle="dropdown" href=""><span class="him-label">Guest</span></a>
                            <ul class="dropdown-menu dm-icon pull-right">
                                <li class="hidden-xs">
                                    <a data-ma-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i> Toggle Fullscreen</a>
                                </li>
                                <li>
                                    <a class ="cd-signin" href="javascript:void(0);" ng-controller="notLoggedCtrl" ng-click="openDiag()"> Login</a>
                                </li>
                                <li>
                                    <a class ="cd-signup"  href="http://localhost/ctrade/sign_up.php" target="_blank">Create Account</a>
                                </li>
                                <li>
                                    <a href="">Settings</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>

        </header>

        <section id="main">

            <section id="content">
                <div class="container" data-ng-view="" id="ng-view" >

                </div>                        
            </section>


        </section>

        <footer id="footer">
            <marquee>
                <div ng-controller="notLoggedCtrl">
                    <ul class="f-menu">
                            <li ng-repeat=" countrz in marketwatch" ng-bind-html-unsafe="countrz.text" >
                               {{countrz.market_company}} - {{countrz.market_vwap}} ( {{countrz.market_change}} ) 
                            </li>
                    </ul>
                </div>
            </marquee>
        </footer>


        <!-- Page Loader -->
        <div class="page-loader">
            <div class="preloader pls-white">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20" />
                </svg>

                <p>Connecting to C-Trade...</p>
            </div>
        </div>

        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
        
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Libs -->
        <script src="angular/js/angular.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ngStorage/0.3.10/ngStorage.min.js"></script>        
        <script src="js/d3.js"></script>
        <script src="js/nv.d3.js"></script>
        <script src="js/angularjs-nvd3-directives.js"></script>        
        <script src="angular/js/angular-route.min.js"></script>
        <script src="angular/js/angular-animate.min.js" ></script>
        <script src="angular/js/toaster.js"></script>
        <script src="vendors/ngDialog/js/ngDialog.min.js"></script>        
        <script src="angular/app/app.js"></script>
        <script src="angular/app/data.js"></script>
        <script src="angular/app/directives.js"></script>
        <script src="angular/app/authCtrl.js"></script>
        <script src="angular/app/notLoggedCtrl.js"></script>
        <script src="angular/app/LoginCtrl.js"></script>
        <script src="angular/app/dashboardCtrl.js"></script>


        <!-- Javascript Libraries -->

        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/app.js"></script>
    
    

    </body>
  </html>